import express from "express";
import expressSession from "express-session";
import Knex from "knex";
import path from "path";
import grant from 'grant';
// import { isLoggedInHTML } from "./utils/guards";
import http from 'http';
import {Server as SocketIO} from 'socket.io';
import redis from 'redis';
import { isLoggedInClinic, isLoggedInHTML } from './utils/guards'

export const redisClient = redis.createClient();
// const client = redis.createClient(port, host);
// By default redis.createClient() will use 127.0.0.1 and port 6379. If you have a customized ip and and a port use

redisClient.on('connect', function() {
    console.log('Redis client connected');
});

redisClient.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const server = new http.Server(app);
const io = new SocketIO(server);

io.on('connection', function (socket) {
    console.log(`socket id = ${socket.id}`);

});

import dotenv from "dotenv";
dotenv.config();

const knexConfig = require("./knexfile");
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

app.use(
    expressSession({
        secret: "Tecky Academy teaches typescript",
        resave: true,
        saveUninitialized: true,
    })
);


const grantExpress = grant.express({
    "defaults": {
        "origin": "http://success.doctoreasy.xyz",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/api/v1/login/google"
    }
});
app.use(grantExpress as express.RequestHandler);

app.use((req, res, next) => {
    console.log(`method: ${req.method}, path: ${req.path}`);
    next();
});

// Import Service & Controllers
import { AppointmentService } from "./services/AppointmentService";
import { AppointmentController } from "./controllers/AppointmentController";

// Config Service & Controllers
const appointmentService = new AppointmentService(knex);
export const appointmentController = new AppointmentController(appointmentService, io);

// Import Service & Controllers
import { ClinicAppointmentService } from "./services/ClinicAppointmentService";
import { ClinicAppointmentController } from "./controllers/ClinicAppointmentController";

// Config Service & Controllers
const clinicAppointmentService = new ClinicAppointmentService(knex);
export const clinicAppointmentController = new ClinicAppointmentController(clinicAppointmentService, io);

import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';
const userService = new UserService(knex);
export const userController = new UserController(userService);

import { SearchMedicalService } from './services/SearchMedicalService';
import { SearchMedicalController } from './controllers/SearchMedicalController';
const searchMedicalService = new SearchMedicalService(knex);
export const searchMedicalController = new SearchMedicalController(searchMedicalService);

import { TicketService } from './services/TicketService';
import { TicketController } from './controllers/TicketController';
const ticketService = new TicketService(knex);
export const ticketController = new TicketController(ticketService, io);

import { ClinicTicketService } from './services/ClinicTicketService';
import { ClinicTicketController } from './controllers/ClinicTicketController';
const clinicTicketService = new ClinicTicketService(knex);
export const clinicTicketController = new ClinicTicketController(clinicTicketService, io);

import { ClinicMedicalService } from './services/ClinicMedicalRecordService';
import { ClinicMedicalRecordController } from './controllers/ClinicMedicalRecordController';
const clinicMedicalRecordService = new ClinicMedicalService(knex);
export const clinicMedicalRecordController = new ClinicMedicalRecordController(clinicMedicalRecordService);


import { ProfileService } from './services/ProfileService';
import { ProfileController } from './controllers/ProfileController';
const profileService = new ProfileService(knex);
export const profileController = new ProfileController(profileService);

import { MedicalService } from './services/MedicalRecordService';
import { MedicalRecordController } from './controllers/MedicalRecordController';
const medicalService = new MedicalService(knex);
export const medicalRecordController = new MedicalRecordController(medicalService);


import { routes } from "./routes";
import { clinicRoutes } from "./ClinicRoutes";

const API_VERSION = process.env.API_VERSION ?? "/api/v1";


app.use(API_VERSION, routes);

app.use(API_VERSION, isLoggedInClinic, clinicRoutes);
app.use("/clinic",isLoggedInClinic, express.static(path.join(__dirname, "private_clinic")));

app.use(express.static(path.join(__dirname, "public")));
app.use(isLoggedInHTML, express.static(path.join(__dirname, "private")));


app.use((req, res) => {
    // res.sendFile(path.join(__dirname, "404.html"));
    res.redirect("/404.html");
});

const PORT = process.env.PORT || 8080;
// app.listen(PORT, () => {
//     console.log(`[info] listening to port: [${PORT}]`);
// });

server.listen(PORT, () => {
    console.log(`[info] listening to port: [${PORT}]`);
});