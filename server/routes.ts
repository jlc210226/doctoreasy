import express from "express";
import { appointmentRoutes } from "./routes/appointment";
import { searchMedicalRoutes } from "./routes/searchMedical";
import { userRoutes } from "./routes/user";
import { ticketRoutes } from "./routes/ticketRoutes"
import { profileRoutes } from './routes/profileRoutes'
import { medicalRecordRoutes } from "./routes/medicalRecord";

export const routes = express.Router();

routes.use("/", userRoutes);
routes.use("/", appointmentRoutes);
routes.use("/", searchMedicalRoutes);
routes.use('/', ticketRoutes);
routes.use('/', profileRoutes);
routes.use('/', medicalRecordRoutes);