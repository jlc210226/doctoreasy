// @ts-ignore
import { Clinic, Ticket } from './../utils/models';
import { ClinicTicketService } from "../services/ClinicTicketService"
import { Request, Response } from 'express';
import { Server as SocketIO } from 'socket.io';


export class ClinicTicketController {
    constructor(private ticketService: ClinicTicketService, private io: SocketIO) { }

    updateTicketStatus = async (req: Request, res: Response) => {
        try {
            const ticketID = parseInt(req.params.ticketID);
            const statusID = parseInt(req.params.statusID);

            const ticket:Ticket[] = await this.ticketService.updateTicketStatus(ticketID, statusID);

            const clinicID = ticket[0].clinic_id;
            const ticketNumber = ticket[0].ticket_number;

            // const clinicID = await this.ticketService.updateTicketStatus(ticketID, statusID);

            // 如果病人嘅status被醫生update...
            if (statusID !== 1) {
                this.io.emit(`${clinicID}_updateServingTicket`, {
                    id: clinicID,
                    ticketID: ticketID,
                    ticketNumber: ticketNumber,
                    statusID
                });
            }

            // // 再計一次邊個而家要ready喇...
            const readyTickets = await this.ticketService.getReadyTickets(clinicID);

            this.io.emit(`${clinicID}_updateReadyTickets`, {
                id: clinicID,
                readyTickets: readyTickets
            });

            res.json({ message: "success" })

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicGetTicket = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            console.log(clinicID)

            if (!clinicID) {
                res.status(400).json({ message: "User haven't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const data = await this.ticketService.clinicGetTicket(clinicID);

            console.log(data)

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    startTicketQueue = async (req: Request, res: Response) => {

        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            await this.ticketService.startTicketQueue(clinicID);

            res.status(200).json({ message: "Queue started" })

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    stopTicketQueue = async (req: Request, res: Response) => {

        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            await this.ticketService.stopTicketQueue(clinicID);

            res.status(200).json({ message: "Queue stopped" })

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicGetTodayTickets = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const data = await this.ticketService.clinicGetTodayTickets(clinicID);

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicSetTicketNumber = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;
            const { ticketNumber } = req.body;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            if (ticketNumber <= 0 || ticketNumber == NaN) {
                res.status(400).json({ message: "Invalid number" });
                return;
            }

            const ticketNumberSet = await this.ticketService.clinicSetTicketNumber(clinicID, ticketNumber)

            this.io.emit(`${clinicID}_clinicUpdateNextTicket`, {
                id: clinicID,
                nextTicket: ticketNumberSet
            });

            res.status(200).json({ message: 'Ticket Number Set', ticketNumberSet });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicGetNextTicketNumber = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const nextTicketNumber = await this.ticketService.clinicGetNextTicketNumber(clinicID)

            res.json(nextTicketNumber);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicLogout = async (req: Request, res: Response) => {
        try {
            req.session["user"] = null;
            res.json({ message: "Logged out successfully" })
            return;
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    clinicGetCurrentServingTicket = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const currentServingTickets = await this.ticketService.clinicGetCurrentServingTicket(clinicID);
            
            console.log(currentServingTickets)
            
            res.json(currentServingTickets)

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    clinicGetLastServedTickets = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const lastServed = await this.ticketService.clinicGetLastServedTicket(clinicID);
            res.json(lastServed);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }
}