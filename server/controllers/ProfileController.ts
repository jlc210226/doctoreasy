import { ProfileService } from "../services/ProfileService"
import { Request, Response } from 'express';

export class ProfileController {
    constructor(private profileService: ProfileService) { }


    getClinicInfo = async (req: Request, res: Response) => {
        try {
            const clinicID: any = req.params.clinicID;
            const data = await this.profileService.getClinicInfo(clinicID)

            // console.log(data)
            // console.log(data.clinicInfo)

            if (!data.clinicInfo) {
                res.status(400).json({ message: "Clinic does not exist" });
                return;
            }

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getPatientProfile = async (req: Request, res: Response) => {
        try {
            const patientID = req.session["user"]
            const profile = await this.profileService.getPatientProfile(patientID.id)
            res.json({ profile: profile })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    clinicGetPatientProfile = async (req: Request, res: Response) => {
        try {
            const patientID: any = req.params.patientID;
            const profile = await this.profileService.clinicGetPatientProfile(patientID)
            res.json({ profile: profile })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    getDoctorInfo = async (req: Request, res: Response) => {
        try {
            const doctorID: any = req.params.doctorID;

            console.log(doctorID)
            const data = await this.profileService.getDoctorInfo(doctorID)

            if (!data.doctorProfile) {
                res.status(400).json({ message: "Doctor does not exists" });
                return;
            }

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getDoctorSpecialties = async (req: Request, res: Response) => {
        try {
            const doctorID: any = req.params.doctorID;

            console.log(doctorID)
            const data = await this.profileService.getDoctorSpecialties(doctorID)

            if (!data) {
                res.status(400).json({ message: "Specialties does not exists" });
                return;
            }

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getComment = async (req: Request, res: Response) => {
        try {
            const doctorID: any = req.params.doctorID;
            const commentInfo = await this.profileService.getComment(doctorID)
            // const username = await this.profileService.getUsername(commentInfo[0]["id"])
            console.log(commentInfo)
            res.json({ commentInfo })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    createComment = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"];
            console.log(userID)
            const patientID = await this.profileService.getPatientID(userID["id"])
            const doctorID: any = req.params.doctorID;
            const rating = req.body.grading;
            const comment = req.body.comment;
            await this.profileService.createComment(doctorID, comment, patientID[0].id)
            const meanRating = await this.profileService.getRating(doctorID);
            const commentCount = await this.profileService.getCommentCount(doctorID)
            await this.profileService.updateCommentCount(doctorID, commentCount[0]["comment_count"] + 1)
            const updatedcommentCount = await this.profileService.getCommentCount(doctorID)
            const oldMeanRating = parseInt(meanRating[0]["rating"])
            const oldCommentCount = parseInt(commentCount[0]["comment_count"])
            const newCommentCount = parseInt(updatedcommentCount[0]["comment_count"])
            const newRating = (oldMeanRating * oldCommentCount + parseInt(rating)) / newCommentCount
            await this.profileService.updateRating(doctorID, newRating)
            res.json({ message: "success" })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

    }

    checkQueueActive = async (req: Request, res: Response) => {
        try {
            const clinicID: any = req.params.clinicID;
            const status = await this.profileService.checkQueueActive(clinicID);
            res.json(status)
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    bookmarkDoctor = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"];
            console.log(userID)
            const patientID = await this.profileService.getPatientID(userID["id"])
            const doctorID: any = req.params.doctorID;
            await this.profileService.bookmarkDoctor(doctorID, patientID[0]["id"])
            res.json({ message: 'success' })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    getBookmarkDoctor = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"];
            console.log(userID)
            const patientID = await this.profileService.getPatientID(userID["id"])
            const doctorlist = await this.profileService.getBookmarkDoctor(patientID[0]["id"])
            console.log(`doctorlist ${doctorlist}`)
            res.json({ doctorlist })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    checkBookmarkDoctor = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"];
            const patientID = await this.profileService.getPatientID(userID["id"])
            const doctorID: any = req.params.doctorID;

            const result = await this.profileService.checkBookmarkDoctor(doctorID, patientID[0]["id"])
            if (result.length > 0) {
                res.json({ message: "already bookmarked" })
            } else {
                res.json({ message: "no bookmark record" })
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }

    }

    cancelBookmark = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"];
            const patientID = await this.profileService.getPatientID(userID["id"])
            const doctorID: any = req.params.doctorID;
            console.log("hiiiiiiiiiiiiiii")
            await this.profileService.cancelBookmark(patientID[0].id, doctorID)
            res.json({message: 'success'})
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    doctorGetPatientInfo = async (req: Request, res: Response) => {
        try {
            const patientID: any = req.params.patientID;
            const userID = req.session["user"];
            const doctorInfo = await this.profileService.getDoctorName(userID["id"])
            const doctor_name = doctorInfo[0]["name"]
            const patientInfo = await this.profileService.getPatientProfileById(patientID)
            console.log(`test:${patientInfo}`)
            res.json({ doctor_name: doctor_name, patientInfo: patientInfo })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
    checkQueueActiveForClinic = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const userRole = req.session["user"]?.role;

            console.log(clinicID)

            if (!clinicID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            if (!userRole) {
                res.status(400).json({ message: "Invalid user" });
                return;
            }

            const status = await this.profileService.checkQueueActive(clinicID)

            res.json(status)

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
}