import { Request, Response } from "express";
import { SearchMedicalService } from "../services/SearchMedicalService"
import { Doctor } from "../utils/models";

export class SearchMedicalController {
    constructor(private searchMedicalService: SearchMedicalService) { }

    getTopDoctors = async (req: Request, res: Response) => {
        try {
            const topDoctorProfiles = await this.searchMedicalService
                .getTopDoctors();

            res.json({ message: "Success", topDoctorProfiles });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    };

    searchDoctor = async (req: Request, res: Response) => {
        try {
            const orderby = req.body.orderby;
            const district = req.body.district;
            const specialty = req.body.specialty;
            const keywords = req.body.keywords;
            console.log(req.body)

            const resultDoctors = (await this.searchMedicalService.searchDoctor(orderby, district, specialty, keywords)) as Doctor[];

            res.json({ message: "Success", resultDoctors });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    };

    searchClinic = async (req: Request, res: Response) => {
        try {
            const district = req.body.district;
            const keywords = req.body.keywords;
            const resultClinic = await this.searchMedicalService.searchClinic(district,keywords)

            console.log(req.body)

            console.log(resultClinic)

            res.json({ message: "Success", resultClinic });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    };

    searchPatient = async (req: Request, res: Response) => {
        try {
            const clinicID = req.session["user"]?.clinic_id;
            const keywords = req.body.keywords;
            const resultKeywords = await this.searchMedicalService.searchPatient(clinicID, keywords);
            res.json({ resultKeywords });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    };
}
