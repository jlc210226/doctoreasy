import { Request, Response } from "express";
import { AppointmentService } from "../services/AppointmentService";
// import { checkPassword } from "../utils/hash";
import { Server as SocketIO } from 'socket.io';

export class AppointmentController {
    constructor(private appointmentService: AppointmentService, private io: SocketIO) { }

    createAppointment = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"]?.id;
            const newSubmit = req.body;
            const doctorID = parseInt(req.params.id);
            console.log(`hihi ${doctorID}`)
            // const userID = req.session['user'].id;
            const date = newSubmit.date;
            const timeSlot = newSubmit.time;
            const formData = { doctorID, date, timeSlot };
            console.log(formData)
            const data:any = await this.appointmentService.createAppointment(
                userID,
                doctorID,
                date,
                timeSlot
            );
            console.log('here')
            console.log(data)
            console.log('there')

            if (data.message){
                res.json(data)
                return;
            }

            if (!userID) {
                res.status(401).json({ message: "Please login in to proceed!" });
                return;
            }

            if (!data) {
                res.status(400).json({ message: "Please pick timeslot and date to proceed!" });
                return;
            }

            // Socket IO
            this.io.emit(`${data.appointmentInfo.clinic_id}_updateNewAppointment`, {
                id: data.appointmentInfo.clinic_id,
                patient_name: data.patientInfo.name, //
                appointment_number: data.appointmentInfo.appointment_number,
                date_of_booking: data.appointmentInfo.date_of_booking, //
                time_of_booking: data.appointmentInfo.time_of_booking,
                doctor_name: data.clinicID.name, //
                patient_id: data.appointmentInfo.patient_id,
                status_id: data.appointmentInfo.status_id,
                status_name: data.appointmentInfo.status
            });

            res.json({ data });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "internal server error" });
        }
    };

    loadTimeSlot = async (req: Request, res: Response) => {
        try {
            const doctorID = parseInt(req.params.id);
            // const selectDate = req.body.whichDay;
            const timeSlot = await this.appointmentService.getTimeSlot(doctorID);

            if (!timeSlot) {
                res.status(400).json({ message: "Results does not exist" });
                return;
            }

            res.json({ timeSlot });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadDoctorInfo = async (req: Request, res: Response) => {
        try {
            const doctorID = parseInt(req.params.id);
            const doctorInfo = await this.appointmentService.getDoctorInfo(doctorID);

            if (!doctorInfo) {
                res.status(400).json({ message: "Selected doctor does not exist" });
                return;
            }

            res.json({ doctorInfo });

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadAppointmentInfo = async (req: Request, res: Response) => {
        try {
            const appointmentID = parseInt(req.params.id);
            console.log(`[controller] ${appointmentID}`)
            console.log('hihihi')
            const appointmentInfo = await this.appointmentService.getAppointmentInfo(appointmentID);

            if (!appointmentInfo) {
                res.status(400).json({ message: "Appointment does not exist" });
                return;
            }
            
            res.json({ appointmentInfo });
            console.log(`[controller] ${appointmentInfo}`)

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadAllDoctorAppointment = async (req: Request, res: Response) => {
        try {
            const doctorID = parseInt(req.params.id);
            const doctorTimeBooked = await this.appointmentService.getAllDoctorAppointment(doctorID);
            res.json({ doctorTimeBooked });
            console.log(doctorTimeBooked)

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loadAllAppointment = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"]
            console.log(`userid: ${userID.id}`)
            const appointmentInfo = await this.appointmentService.getAllAppointment(userID.id);
            console.log(appointmentInfo)
            res.json({ appointmentInfo });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    getCurrentServingAppointments = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);

            const currentServingAppointments = await this.appointmentService.getCurrentServingAppointments(clinicID);
            res.json(currentServingAppointments);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    getlastServedAppointments = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);

            const lastServedAppointments = await this.appointmentService.getLastServedAppointments(clinicID);
            res.json(lastServedAppointments);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    loadAppointmentInfoforClinic = async (req: Request, res: Response) => {
        try {
            // const clinicID = parseInt(req.params.id);
            const clinicID = req.session["user"]?.clinic_id;
            // console.log(`bye ${clinicID}`)

            const appointmentInfoforClinic = await this.appointmentService.getAppointmentInfoforClinic(clinicID);

            res.json({appointmentInfoforClinic});

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

}
