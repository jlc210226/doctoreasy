import { Request, Response } from "express";
import { ClinicMedicalService } from "../services/ClinicMedicalRecordService";

export class ClinicMedicalRecordController {
    constructor(private CLinicMedicalService: ClinicMedicalService) { }

    clinicGetAllMedicalRecords = async (req: Request, res: Response) => {
        try {
            const patientID: any = req.params.patientID;
            const medicalReports = await this.CLinicMedicalService.clinicGetAllMedicalRecord(patientID)
            res.json({ medicalReports })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    createMedicalRecord = async (req: Request, res: Response) => {
        try{
            const patientID: any = req.params.patientID;
            const doctorUserID = req.session["user"];
            const clinicID = await this.CLinicMedicalService.getClinicIDByDoctorID(doctorUserID.id)
            const doctorProfileID = await this.CLinicMedicalService.getDoctorProfileIDbyUserID(doctorUserID.id)
            const report_date = req.body.report_date;
            const symptoms = req.body.symptoms;
            const diagnosis = req.body.diagnosis;
            const medication = req.body.medications;
            const remarks = req.body.remarks
            await this.CLinicMedicalService.createMedicalRecord(patientID, doctorProfileID.id, clinicID[0].clinic_profile_id, report_date, symptoms, diagnosis, medication, remarks)
            res.json({message: "success"})
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }
}