import { TicketService } from "../services/TicketService"
import { Request, Response } from 'express';
import { Server as SocketIO } from 'socket.io';


export class TicketController {
    constructor(private ticketService: TicketService, private io: SocketIO) { }

    getCurrentServingTickets = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);

            const currentServingTickets = await this.ticketService.getCurrentServingTicket(clinicID);
            res.json(currentServingTickets)

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getLastServedTickets = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);

            const lastServed = await this.ticketService.getLastServedTickets(clinicID);
            res.json(lastServed);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getReadyTickets = async (req: Request, res: Response) => {
        try {

            const clinicID = parseInt(req.params.clinicID);

            const readyTickets = await this.ticketService.getReadyTickets(clinicID)

            res.json(readyTickets);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getYourTicketNumber = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);

            const yourTicketNumber = await this.ticketService.getYourTicketNumber(clinicID)

            res.json(yourTicketNumber);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getNewTicket = async (req: Request, res: Response) => {
        try {
            const clinicID = parseInt(req.params.clinicID);
            const userID = req.session["user"]?.id;

            if (!userID) {
                res.status(401).json({ message: 'user does not exist' });
                return;
            }

            const result: any = await this.ticketService.getNewTicket(clinicID, userID)

            console.log(result["ticketID"][0])

            const ticket = await this.ticketService.getTicketAllInfo(result["ticketID"][0])

            console.log("Get New Ticket")
            console.log(result)

            this.io.emit(`${clinicID}_updateNextTicket`, {
                id: clinicID,
                nextTicket: result.nextTicket,
                ticket
            });

            res.json(result.ticketID);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getTicketInfo = async (req: Request, res: Response) => {
        try {
            const ticketID = req.params.ticketID;
            // const userID = req.session["user"]?.id;

            const data = await this.ticketService.getTicketInfo(ticketID)

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    checkGetTicketNot = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"]?.id;

            if (!userID) {
                res.status(400).json({ message: "User havn't logged in yet" });
                return;
            }

            const data = await this.ticketService.checkGetTicketNot(userID);

            res.json(data);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

    getOwnTicket = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"]?.id;
           
            console.log(userID)

            if (!userID) {
                res.status(400).json({ message: "Invalid Access" });
                return;
            }

            const ticket = await this.ticketService.getOwnTicket(userID);

            res.json(ticket);

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

}