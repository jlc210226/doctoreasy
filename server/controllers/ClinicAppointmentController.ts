// @ts-ignore
// import { Clinic } from '../utils/models';
import { ClinicAppointmentService } from "../services/ClinicTicketService"
import { Request, Response } from 'express';
import { Server as SocketIO } from 'socket.io';


export class ClinicAppointmentController {
    constructor(private appointmentService: ClinicAppointmentService, private io: SocketIO) { }

    updateAppointmentStatus = async (req: Request, res: Response) => {
        try {
            const appointmentID = parseInt(req.params.appointmentID);
            const statusID = parseInt(req.params.statusID);
            console.log(appointmentID)
            console.log(statusID)
            console.log('here')
            const appointment = await this.appointmentService.updateAppointmentStatus(appointmentID, statusID);

            console.log(appointment)

            const clinicID = appointment.clinic_id

            if (statusID === 2){
                const arrivedAppointment = await this.appointmentService.getAppointmentByappointmentID(appointmentID)
                console.log(clinicID)
                console.log('updated')
                this.io.emit(`${clinicID}_updateServingTicket`, {
                    id: clinicID,
                    appointmentID: appointmentID,
                    appointment: arrivedAppointment,
                    statusID
                });
            }else if (statusID !== 1) {
                console.log(clinicID)
                console.log('updated')
                this.io.emit(`${clinicID}_updateServingTicket`, {
                    id: clinicID,
                    appointmentID: appointmentID,
                    statusID
                });
            }

            res.json({ message: "success" })

        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' })
        }
    }

}