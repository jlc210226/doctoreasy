import { Request, Response } from "express";
import { MedicalService } from "../services/MedicalRecordService";
import {  medicalReportData } from "../utils/models";

export class MedicalRecordController {
    constructor(private MedicalService: MedicalService) { }

    getAllMedicalRecords = async (req: Request, res: Response) => {
        try {
            const userID = req.session["user"]
            const medicalReports = await this.MedicalService.getAllMedicalRecord(userID.id)
            console.log(medicalReports)
            res.json({ medicalReports })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    //Moved to clinicmedicalrecord
    // clinicGetAllMedicalRecords = async (req: Request, res: Response) => {
    //     try {
    //         const patientID: any = req.params.patientID;
    //         const medicalReports = await this.MedicalService.clinicGetAllMedicalRecord(patientID)
    //         res.json({ medicalReports })
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // }
    getOwnMedicalRecord = async (req: Request, res: Response) => {
        try {
            const medicalReportID = parseInt(req.params.medicalReportId);
            const medicalReportData = (await this.MedicalService.getOwnMedicalRecord(medicalReportID))[0]
            const medicalReport: medicalReportData = {
                patient_name: medicalReportData.patient_name,
                gender: medicalReportData.gender,
                report_date: medicalReportData.report_date,
                diagnosis: medicalReportData.diagnosis,
                doctor_name: medicalReportData.doctor_name,
                symptoms: medicalReportData.symptoms,
                remarks: medicalReportData.remark,
                medication: medicalReportData.medication
            }
            console.log(medicalReport)
            res.json({ medicalReport })
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    //Moved to clinic medical record
    // createMedicalRecord = async (req: Request, res: Response) => {
    //     try{
    //         const patientID: any = req.params.patientID;
    //         const doctorUserID = req.session["user"];
    //         const clinicID = await this.MedicalService.getClinicIDByDoctorID(doctorUserID.id)
    //         const doctorProfileID = await this.MedicalService.getDoctorProfileIDbyUserID(doctorUserID.id)
    //         const report_date = req.body.report_date;
    //         const symptoms = req.body.symptoms;
    //         const diagnosis = req.body.diagnosis;
    //         const medication = req.body.medications;
    //         const remarks = req.body.remarks
    //         await this.MedicalService.createMedicalRecord(patientID, doctorProfileID.id, clinicID[0].clinic_profile_id, report_date, symptoms, diagnosis, medication, remarks)
    //         res.json({message: "success"})
    //     } catch (err) {
    //         console.error(err.message);
    //         res.status(500).json({ message: 'internal server error' });
    //     }
    // }
}