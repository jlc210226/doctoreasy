import { UserService } from "../services/UserService";
import { Request, Response } from 'express';
import { checkPassword } from '../utils/hash';
import fetch from "node-fetch"

export class UserController {
    constructor(private userService: UserService) { }

    login = async (req: Request, res: Response) => {
        try {
            const { username, password } = req.body;
            const user = await this.userService.getUserByUsername(username);
            if (!user) {
                res.status(400).json({ message: "invalid username/password" })
                return
            }
            const match = await checkPassword(password, user.password);
            if (!match) {
                res.status(400).json({ message: 'invalid username/password' });
                return;
            }
            req.session['user'] = { id: user.id };
            res.json({ message: 'success' });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    cliniclogin = async (req: Request, res: Response) => {
        try {
            const { username, password } = req.body;
            const user = await this.userService.getMedicalUserByUsername(username);
            console.log(user)
            if (!user) {
                res.status(400).json({ message: "invalid username/password" })
                return
            }
            const match = await checkPassword(password, user.password);
            if (!match) {
                res.status(400).json({ message: 'invalid username/password' });
                return;
            }
            req.session['user'] = { id: user.id , role: user.role , clinic_id: user.clinic_profile_id, username: user.username };

            

            res.json({ message: 'success' });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        try {
            const accessToken = req.session?.['grant'].response.access_token;
            const fetchRes = await fetch(
                'https://www.googleapis.com/oauth2/v2/userinfo',
                {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                }
            );
            const result = await fetchRes.json();
            let user = await this.userService.getUserByUsername(result.email);

            if (!user) {
                // generate random password
                const insertedID = await this.userService.createUser(
                    result.email,
                    'jasonisveryhandsomeandnice'
                );
                user = { id: insertedID } as any;
            }
            req.session['user'] = { id: user?.id };
            res.redirect('/index.html');
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    registerAndCreateProfile = async (req: Request, res: Response) => {
        try {
            const username = req.body.username;
            const password = req.body.password;
            const userID = await this.userService.createUser(username, password);
            // console.log(`userIDDDDDDD`)
            // console.log(userID)
            const name = req.body.id_name;
            const date_of_birth = req.body.birthday;
            const address = req.body.address;
            const gender = req.body.gender;
            const id_number = req.body.id_number;
            const phone = req.body.phone;
            const email = req.body.email;
            const profileID = await this.userService.createProfile(userID, name, date_of_birth, address, gender, id_number, phone, email)
            // req.session['user'] = { id: userID };
            // console.log(req.session['user'].id)
            res.json({ message: 'success', profileID: profileID });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    checkLogin = async (req: Request, res: Response) => {
        try {
            if (req.session["user"]) {
                res.json({ message: "LoggedIn" })
                return;
            }
            res.json({ message: "Please Login" })
            return;

        } catch (err) {
            console.log(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    logout = async (req: Request, res: Response) => {
        try {
            req.session["user"] = null;

            res.json({ message: "Logged out successfully" })

            return;
        } catch (err) {
            console.log(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    getUserInfo = async (req: Request, res: Response) => {
        try {
            let userInfo = req.session["user"]
            res.json({ userInfo })
            return;

        } catch (err) {
            console.log(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

}