export const TICKET_STATUS = Object.freeze({
    BOOKED: 1,
    SEEING_DOCTOR: 2,
    COMPLETED: 3,
    NO_SHOW: 4
});

export const BOOKING_STATUS = Object.freeze({
    BOOKED: 1,
    ARRIVED: 2,
    SEEING_DOCTOR: 3,
    COMPLETED: 4,
    CANCELLED: 5,
    NO_SHOW: 6
});
