export interface User {
    id: number;
    username: string;
    password: string;
    role?: string;
    clinic_profile_id?: number;
}

export interface Doctor {
    id?: number;
    name: string;
    gender: string;
    registration_no: string;
    profile_pic: string;
    user_medical_id: number;
    rating: number;
    comment_count: number;
    specialties?: any;
    district?: string;
    office_hour?: any;
    opening_hours?: string;
    clinic_name?: string; 
    clinic_id?: number;
    clinic_address?: string;
}


export interface Clinic {
    id?: number;
    name: string;
    address: string;
    district: string;
    phone: string;
    opening_hours: string;
    queue_active: boolean;
    tickets?: any;
}

export interface patientProfile {
    id: number,
    name: string,
    date_of_birth: Date,
    address: string,
    gender: string,
    id_number: string,
    phone: string,
    email: string
}

export interface doctorSpecialty {
    name: string;
    specialty: string
}
export interface officeHourData {
    name: string;
    day: number;
    period: string;
    start: string;
    end: string;
}

export interface diagnosisReportData{
    patient_name: string;
    clinic: string;
    doctor_name: string;
    report_date: Date;
    symptoms: string;
    diagnosis: string;
    medication: string;
    remark: string
}

export interface medicalRecord{
    name: string,
    diagnosis: string,
    report_date: Date
}

export interface bookingRecord{
    ticket_number: number;
    appointment_number: number;
    date_of_booking: string;
    time_of_booking: string;
}

export interface doctorOpeningHour{
    day: number;
    start: string;
    end: string;
}

export interface bookMarkedData {
    doctor_name: string;
    patient_name: string;
}
export interface comment_Data {
    doctor_name: string;
    patient_name: string;
    comment: string;
    doctor_rating: number;
}

export interface medicalReportData{
    patient_name: string;
    gender: string;
    report_date: Date;
    diagnosis: string;
    doctor_name: string;
    symptoms: string;
    remarks: string;
    medication: string;
}

export interface Ticket {
    id: number;
    ticket_number: string;
    patient_id: number;
    clinic_id: number;
    status_id: number;
}