import { Request, Response, NextFunction } from "express";

export function isLoggedInAPI(req: Request, res: Response, next: NextFunction) {
    if (req.session?.["user"]) {
        next();
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
}

export function isLoggedInHTML(
    req: Request,
    res: Response,
    next: NextFunction
) {
    console.log("html")
    if (req.session?.["user"]) {
        next();
    } else {
        res.redirect("/login.html");
    }
}

export function isLoggedInClinic(
    req: Request,
    res: Response,
    next: NextFunction
) {
    console.log("clinic")

    if (req.session?.["user"] && req.session?.["user"].role) {
        next();
    } else {
        res.redirect("/clinicLogin.html");
    }
}
