window.onload = function () {
    loadAndDisplayTicket()
}

async function loadAndDisplayTicket() {

    const urlParams = new URLSearchParams(window.location.search);
    const ticketIDString = urlParams.get("ticketID");

    const decodedTicketID = atob(ticketIDString);
 
    const res = await fetch(`/api/v1/getTicketInfo/${decodedTicketID}`);

    const data = await res.json();

    const clinicID = data.info[0].clinic_id

    let doctorNames = data.doctorList.map(function (doctorList) {
        return doctorList['name'];
    });

    doctorNames = doctorNames.join(' ');

    dateformat = moment.tz(data.info[0].created_at, "Asia/Hong_Kong");

    finalDate = dateformat.format("YYYY-MM-DD h:mm:ss a")

    let clinicStr = `
    <div class="icon"><i class="fas fa-clinic-medical"></i></div>
                        <div class="clinic_name">${data.info[0].name}</div>
                        <div class="doctor_name">${doctorNames}</div>
                        <div class="address">
                            <div class="info_title">地址：</div>
                            <div class="info_content">${data.info[0].address}</div>
                        </div>

                        <div class="contact">
                            <div class="info_title">聯絡電話：</div>
                            <div class="info_content">${data.info[0].phone}</div>
                        </div>

                        <div class="opening_hours">
                            <div class="info_title">開放時間：</div>
                            <div class="info_content">
                            ${data.info[0].opening_hours}
                            </div>
                        </div>
    `
    document.querySelector('.clinic_info').innerHTML = clinicStr;

    let ticketStr = `
    <div class="message">你已成功取籌！</div>
                        <div class="ticket">


                            <div>你的籌號</div>
                            <div class="current_number">${data.info[0].ticket_number}</div>
                        </div>
                        <div class="get_time">取籌時間：${finalDate}</div>
    `

    document.querySelector('.your_ticket_container').innerHTML = ticketStr;

    document.querySelector('.button-group').innerHTML =
    `
    <button class="screen_cap" onclick="screenCap()">截圖</button>
    <button class="ticket_status" onclick="window.location.href='/getTicket.html?clinicID=${clinicID}'">查看籌號狀態</button>
    `

}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");

        return;
    }

    const ticketID = data[0].id;

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}

function screenCap() {
    html2canvas(document.querySelector('body'), {
        onrendered: function (canvas) {
            return Canvas2Image.saveAsPNG(canvas);
        }
    });
};

