window.onload = () => {
    loadDoctorList()
}


async function loadDoctorList() {
    const res = await fetch(`/api/v1/getBookmarkDoctor/`)
    const data = await res.json();
    console.log(data.doctorlist)
    if (data.doctorlist) {
        let doctorlistStr = ``
        for (let list of data.doctorlist) {
            let roundedRating = Math.round(list.rating)
            let stars = ``;
            let x = 0;
            while (x < 5) {
                if (x < roundedRating) {
                    stars += '<span class="fa fa-star checked"></span>'
                } else {
                    stars += '<span class="fa fa-star"></span>'
                }
                x++;
            }
            let specialties = []
            for (let specialty of list.specialties) {
                specialties.push(specialtyToChinese(specialty.specialty))
            }
            console.log(specialties)
            doctorlistStr += ` <div class="doctors">
            <img src="./img/doctor_dummy_img.png" alt="doctor lee" height="100" width="100">
            <div class="doctor-profile"><span class="doctor-name">${list.name}</span><br>
                <span class="specialty">${specialties}</span><br>
                <span class="rating">${stars}</span>
                <div class="doctor-buttons">
                    <button onclick="window.location.href='/doctorProfile.html?doctorID=${list.id}'">
                        詳情
                    </button>
                    <button class="delete" onclick="cancelBookmark(${list.id})">
                    取消收藏
                </button>
                </div>
            </div>
        </div>`
        }
        document.querySelector(".doctor-list").innerHTML = doctorlistStr
    }else {
        console.log(data.message)
    }
}

async function cancelBookmark (doctorID) {
    const res = await fetch(`/api/v1/cancelBookmark/${doctorID}`)
    const data = await res.json();
    if(res.status === 200) {
        document.querySelector(".delete").style.display = "none"
        await loadDoctorList()
    }else {
        console.log(data.message)
    }
}
async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}