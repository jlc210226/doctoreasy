window.onload = () => {
    getPatientProfile()
    getBookingRecord()
    getAllMedicalRecord()
}


async function getPatientProfile() {
    const res = await fetch("/api/v1/patientProfile/")
    const data = await res.json();
    console.log(data)
    if (data.profile.name) {
        dateformat = moment.tz(data.profile.date_of_birth, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")
        let profileInfoStr = `
        <span>姓名：</br>${data.profile.name} </span>
        </br><span>地址：</br>${data.profile.address}</span>
        </br><span>性別：</br>${data.profile.gender}</span>
        </br><span>出生日期：</br>${finalDate}</span>
        </br><span>身份證號碼：</br>${data.profile.id_number}</span>
        </br><span>電話：</br>${data.profile.phone}</span>
        </br><span>電郵：</br>${data.profile.email}</span>`
        const profileContainer = document.querySelector(".personal-info")
        profileContainer.innerHTML = profileInfoStr
    } else {
        console.log(data.message)
    }
}

async function getBookingRecord() {
    const res = await fetch("/api/v1/getAllAppoinment/")
    const data = await res.json();
    console.log(data)
    if (data.appointmentInfo.length > 0) {
        let profileInfoStr = `<h6>預約</h6>`
        for (let singleData of data.appointmentInfo) {
            dateformat = moment.tz(singleData.date_of_booking, "Asia/Hong_Kong");
            finalDate = dateformat.format("YYYY-MM-DD")
            profileInfoStr += `  <div onclick="window.location.href='/appointment_confirmation.html?id=${singleData.id}'" class="booking-record"><span class="date">預約日期：${finalDate}</span>
        </br>預約時段：<span class="time">${singleData.time_of_booking}</span>
        </br>診所名稱：<span class="clinic-name">${singleData.name}</span></div>`
        }
        console.log(profileInfoStr)
        document.querySelector(".booking-container").innerHTML = profileInfoStr
    } else if (data.appointmentInfo.length == 0) {
        document.querySelector(".booking-container").innerHTML = ``
    }
    else {
        console.log(data.message)
    }
}

async function getAllMedicalRecord() {
    const res = await fetch("/api/v1/getAllMedicalRecord/")
    const data = await res.json();
    console.log(data)
    if (data.medicalReports.length > 0) {
        let medicalInfoStr = `<h6>醫療記錄</h6>`
        for (let singleData of data.medicalReports) {
            dateformat = moment.tz(singleData.report_date, "Asia/Hong_Kong");
            finalDate = dateformat.format("YYYY-MM-DD")
            medicalInfoStr += `<div onclick="window.location.href='/medicalReport.html?medicalReportId=${singleData.id}'" class="medical-record"><span>診斷日期：${finalDate}</span>
            </br><span>診斷結果：${singleData.diagnosis}</span>
            </br><span>醫生：${singleData.name}</span></div>`
        }
        console.log(medicalInfoStr)
        document.querySelector(".medical-record-container").innerHTML = medicalInfoStr
    } else if (data.medicalReports.length == 0) {
        document.querySelector(".medical-record-container").innerHTML = ``
    }
    else {
        console.log(data.message)
    }
}

async function logout() {

    console.log("logout")

    const res = await fetch(`/api/v1/logout`);

    const result = await res.json();

    if (res.status === 200) {
        alert(result.message);
        window.location = "/index.html"
    } else {
        alert(result.message)
    }

}


// ticket button to redirect to ticket

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}


document.querySelector('.own-ticket').addEventListener('click', () => {
    getOwnTicket()
})

