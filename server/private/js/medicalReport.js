window.onload = () => {
    getMedicalReport()
}

async function getMedicalReport() {
    const urlParams = new URLSearchParams(window.location.search);
    const medicalReportId = urlParams.get("medicalReportId")
    console.log(medicalReportId)
    const res = await fetch(`/api/v1/getMedicalReport/${medicalReportId}/`)
    const data = await res.json();
    console.log(data)
    if (data.medicalReport) {
        dateformat = moment.tz(data.medicalReport.report_date, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")
        let medicalInfoStr = `病人姓名：<div class="grey-container patient-name">${data.medicalReport.patient_name}</div>
            病人性別：<div class="grey-container patient-gender">${data.medicalReport.gender}</div>
            診斷日期：<div class="grey-container date">${finalDate}</div>
            診斷結果：<div class="grey-container diagnosis">${data.medicalReport.diagnosis}</div>
            醫生： <div class="grey-container doctor">${data.medicalReport.doctor_name}</div>
            病徵：<div class="grey-container symptoms">${data.medicalReport.symptoms}</div>
            備註： <div class="grey-container remarks">${data.medicalReport.remarks}</div>
            藥物： <div class="grey-container medication">${data.medicalReport.medication}</div>`
            document.querySelector(".medical-record").innerHTML = medicalInfoStr
    } else {
        console.log(data.message)
    }

}