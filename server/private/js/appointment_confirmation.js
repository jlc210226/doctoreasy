
window.onload = () => {
    loadAppointmentInfo();
};

// Screenshot function
document.querySelector('.save').addEventListener('click', function () {
    console.log('screenshot!')
    html2canvas(document.body, {
        onrendered: function (canvas) {
            // document.body.appendChild(canvas);
            return Canvas2Image.saveAsPNG(canvas);
        }
    });
});

// Add to Calendar
document.querySelector('.addeventatc').addEventListener('click', function () {
    console.log('add to calendar!')

    let time = document.querySelector(".time").innerHTML;
    let date = document.querySelector(".date").innerHTML;
    let timezone = 'Asia/Hong_Kong';
    let title = '已預約睇醫生';
    let description = '已預約睇醫生';
    let location = document.querySelector(".clinic-address").innerHTML;

    document.querySelector(".addeventatc").innerHTML = 
    `<span class="addcaltext">添加到行事曆</span>
    <span class="start">${date}</span>
    <span class="end">${date}</span>
    <span class="timezone">${timezone}</span>
    <span class="title">${title}</span>
    <span class="description">${description}</span>
    <span class="location">${location}</span>
    </div>`

});

// // Fetch appointment info and render on the screen
async function loadAppointmentInfo() {
    console.log('hi')
    const urlParams = new URLSearchParams(window.location.search);
    const appointmentID = urlParams.get("id")
    console.log(appointmentID)

    const res = await fetch(`api/v1/appointment/${appointmentID}`);
    const result = await res.json();
    console.log(result)
    document.querySelector(".appointment-number").innerHTML = result.appointmentInfo[0].appointment_number;
    document.querySelector(".doctor-name").innerHTML = result.appointmentInfo[0].doctor_name;
    document.querySelector(".doctor-speciality").innerHTML = result.appointmentInfo[0].specialty;

    
    
    // dateformat = moment(result.appointmentInfo[0].date_of_booking, "YYYY-MM-DD").tz('Asia/Hong_Kong').format()
    dateformat = moment.tz(result.appointmentInfo[0].date_of_booking, "Asia/Hong_Kong");
    // console.log(dateformat.unix())
    finalDate = dateformat.format("YYYY-MM-DD")
    document.querySelector(".date").innerHTML = finalDate;

    document.querySelector(".time").innerHTML = result.appointmentInfo[0].time_of_booking;

    document.querySelector(".clinic-name").innerHTML = result.appointmentInfo[0].clinic_name;
    document.querySelector(".clinic-contact").innerHTML = result.appointmentInfo[0].phone;
    document.querySelector(".clinic-address").innerHTML = result.appointmentInfo[0].address;
    // appointmentNumber = result.appointment_number;
    // date = result.date_of_booking;
    // time = result.time_of_booking;

    // console.log('hi')
    // const res2 = await fetch('/clinic');
    // const result2 = await res2.json();
    // clinicName = result2.name;
    // clinicContact = result2.phone;
    // clinicAddress = result2.address;
}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}