import xlsx from "xlsx";

async function main() {
    const file = xlsx.readFile('./test5.xlsx')

    let doctorNamesArray: string[] = []
    for (let i = 0; i < 30; i++) {
        doctorNamesArray.push(`doctor${i + 1}`)
    }

    const doctorProfiles = [];

    for (let i = 0; i < 30; i++) {
        if (i % 2 === 0) {
            doctorProfiles.push({ id: i + 1, name: doctorNamesArray[i], gender: "female", registration_no: `M02345${i}`, user_medical_id: i, created_at: "2016-06-06 01:00:00", updated_at: "2016-06-06 01:00:00" })
        } else {
            doctorProfiles.push({ id: i + 1, name: doctorNamesArray[i], gender: "male", registration_no: `M02345${i}`, user_medical_id: i, created_at: "2016-06-06 01:00:00", updated_at: "2016-06-06 01:00:00" })
        }
    }

    const ws = xlsx.utils.json_to_sheet(doctorProfiles)
    xlsx.utils.book_append_sheet(file, ws, "Sheet2");

    // Writing to our file 
    xlsx.writeFile(file, './test5.xlsx')
}

main();