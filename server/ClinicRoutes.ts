import express from "express";
import { clinicTicketRoutes } from "./routes/clinicTicketRoutes";
import { clinicMedicalRecordRoutes } from "./routes/clinicRecord";
import { clinicAppointmentRoutes } from "./routes/clinicAppointment";

export const clinicRoutes = express.Router();
clinicRoutes.use('/', clinicTicketRoutes);
clinicRoutes.use('/', clinicMedicalRecordRoutes);
clinicRoutes.use('/', clinicAppointmentRoutes);
