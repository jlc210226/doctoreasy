import * as Knex from "knex";
import path from "path";
import xlsx from "xlsx";
import { hashPassword } from "../utils/hash";
import { commentTableName, patientBookmarkTableName, ticketTableName, bookingRecordTableName, ticketStatusTableName, bookingStatusTableName, doctorOfficeHourTableName, diagnosisReportTableName, doctorSpecialtyTableName, specialtyTableName, doctorProfileTableName, userMedicalTableName, clinicProfileTableName, districtTableName, patientProfileTableName, userPatientTableName } from "../utils/tables";
import { TICKET_STATUS, BOOKING_STATUS } from "../utils/variables"

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(commentTableName).del();
    await knex(patientBookmarkTableName).del();
    await knex(ticketTableName).del();
    await knex(bookingRecordTableName).del();
    await knex(doctorOfficeHourTableName).del();
    await knex(diagnosisReportTableName).del();
    await knex(doctorSpecialtyTableName).del();
    await knex(specialtyTableName).del();
    await knex(doctorProfileTableName).del();
    await knex(userMedicalTableName).del();
    await knex(clinicProfileTableName).del();
    await knex(districtTableName).del();
    await knex(patientProfileTableName).del();
    await knex(userPatientTableName).del();
    await knex(ticketStatusTableName).del();
    await knex(bookingStatusTableName).del();

    // read workbook
    const workbook = xlsx.readFile(
        path.join(__dirname, "c13-bad-project-diagnosisdata.xlsx")
    );


    // reading sheets

    const userPatients: Array<{ username: string; password: string; }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_patient"]);
    const districts: Array<{ district: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["district"]);
    const patientProfiles: Array<{ username: string; name: string; date_of_birth: Date; address: string; gender: string; id_number: string; phone: string; email: string; created_at: Date; updated_at: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["patient_profile"]);
    const clinicProfiles: Array<{ name: string; address: string; phone: string; district: string; opening_hour: string; queue_active: string; created_at: Date; updated_at: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["clinic_profile"]);
    const userMedical: Array<{ username: string; password: string; clinic_name: string, role: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["user_medical"]);
    const doctorProfiles: Array<{ name: string; clinic: string; gender: string; registration_no: string; user_medical_id: string; rating: number; comment_count: number }> = xlsx.utils.sheet_to_json(workbook.Sheets["doctor_profile"]);
    const specialty: Array<{ specialty: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["specialty"]);
    const doctorSpecialty: Array<{ name: string; specialty: string; }> = xlsx.utils.sheet_to_json(workbook.Sheets["doctor_specialty"]);
    const doctorOfficeHour: Array<{ name: string; day: string; period: string, start: string, end: string }> = xlsx.utils.sheet_to_json(workbook.Sheets["doctor_office_hour"], {raw: true});
    const bookingRecord: Array<{ ticket_number: number; appointment_number: string; patient_profile_id: string; doctor_profile_id: string; date_of_booking: Date; time_of_booking: string; status: string}> = xlsx.utils.sheet_to_json(workbook.Sheets["booking_record"], {raw: false});
    const diagnosis: Array<{ patient_name: string; clinic: string; doctor_name: string; report_date: Date; symptoms: string; diagnosis: string; medication: string; remark: string}> = xlsx.utils.sheet_to_json(workbook.Sheets["diagnosis_report"], {raw: false});
    const ticket: Array<{ ticket_number: string; patient_profile_id: string; clinic_profile_id: string; status_id: string}> = xlsx.utils.sheet_to_json(workbook.Sheets["ticket"]);
    const bookMarkDoctor: Array<{ doctor_name: string; patient_name: string}> = xlsx.utils.sheet_to_json(workbook.Sheets["bookmark_doctor"]);
    const comment: Array<{ doctor_name: string; patient_name: string; comment: string}>= xlsx.utils.sheet_to_json(workbook.Sheets["comment"]);
    const trx = await knex.transaction();
    try {
        await trx(bookingStatusTableName).insert(Object.keys(BOOKING_STATUS).map(key => ({
            id: BOOKING_STATUS[key],
            status: key
        })));

        await trx(ticketStatusTableName).insert(Object.keys(TICKET_STATUS).map(key => ({
            id: TICKET_STATUS[key],
            status: key
        })));

        // const hashedPassword = await hashPassword("1234");
        for (const row of userPatients) {
            row.password = await hashPassword(row.password);
        }
        const resultUserPatients = (await trx(userPatientTableName).insert(userPatients).returning(["id", "username"])) as Array<{ id: number, username: string }>;
        const userPatientMapping = resultUserPatients.reduce(
            (acc, patient) => acc.set(patient.username, patient.id),
            new Map<string, number>()
        );


        const patientProfileData = patientProfiles.map((patientProfile) => {
            return {
                user_patient_id: userPatientMapping.get(patientProfile["username"]),
                name: patientProfile["name"],
                date_of_birth: patientProfile["date_of_birth"],
                address: patientProfile["address"],
                gender: patientProfile["gender"],
                id_number: patientProfile["id_number"],
                phone: patientProfile["phone"],
                email: patientProfile["email"]
            }
        })

        const resultPatientProfile = (await trx(patientProfileTableName).insert(patientProfileData).returning(["id", "name"])) as Array<{ id: number, name: string }>;


        const patientProfileMapping = resultPatientProfile.reduce(
            (acc, patientProfile) => acc.set(patientProfile.name, patientProfile.id),
            new Map<string, number>()
        )
        console.log(patientProfileMapping)


        const resultDistrict = (await trx(districtTableName).insert(districts).returning(["id", "district"])) as Array<{ id: number, district: string }>
        const districtMapping = resultDistrict.reduce(
            (acc, district) => acc.set(district.district, district.id),
            new Map<string, number>()
        )


        const clinicPofileData = clinicProfiles.map((clinicProfile) => {
            return {
                name: clinicProfile["name"],
                address: clinicProfile["address"],
                phone: clinicProfile["phone"],
                district_id: districtMapping.get(clinicProfile["district"]),
                opening_hours: clinicProfile["opening_hours"],
                queue_active: clinicProfile["queue_active"],
                // created_at: clinicProfiles["created_at"],
                // updated_at: clinicProfiles["updated_at"]
            }
        })

        const resultClinicProfile = (await trx(clinicProfileTableName).insert(clinicPofileData).returning(["id", "name"])) as Array<{ id: number, name: string }>;

        const clinicMapping = resultClinicProfile.reduce(
            (acc, clinic) => acc.set(clinic.name, clinic.id),
            new Map<string, number>()
        )
        

        for (const row of userMedical) {
            row.password = await hashPassword(row.password);
        }

        const userMedicalData = userMedical.map((userMedical) => {
            return {
                username: userMedical["username"],
                password: userMedical["password"],
                clinic_profile_id: clinicMapping.get(userMedical["clinic_name"]),
                role: userMedical["role"]
                // created_at: clinicProfiles["created_at"],
                // updated_at: clinicProfiles["updated_at"]
            }
        })
        const resultUserMedical = (await trx(userMedicalTableName).insert(userMedicalData).returning(["id", "username"])) as Array<{ id: number, username: string }>;
        const userMedicalMapping = resultUserMedical.reduce(
            (acc, medical) => acc.set(medical.username, medical.id),
            new Map<string, number>()
        );
      

        const doctorProfileData = doctorProfiles.map((doctorProfile) => {
            return {
                name: doctorProfile["name"],
                gender: doctorProfile["gender"],
                registration_no: doctorProfile["registration_no"],
                user_medical_id: userMedicalMapping.get(doctorProfile["user_medical_id"]),
                rating: doctorProfile["rating"],
                comment_count: doctorProfile["comment_count"]
            }
        })
        console.log(doctorProfileData)

        const resultDoctorProfile = (await trx(doctorProfileTableName).insert(doctorProfileData).returning(["id", "name"])) as Array<{ id: number, name: string }>;

        const doctorProfileMapping = resultDoctorProfile.reduce(
            (acc, doctorProfile) => acc.set(doctorProfile.name, doctorProfile.id),
            new Map<string, number>()
        );

     

        const resultSpecialty = (await trx(specialtyTableName).insert(specialty).returning(["id", "specialty"])) as Array<{ id: number, specialty: string }>;
        const specialtyMapping = resultSpecialty.reduce(
            (acc, specialty) => acc.set(specialty.specialty, specialty.id),
            new Map<string, number>()
        )

        console.log(specialtyMapping.get("cardiology"))
        const doctorSpecialtyData = doctorSpecialty.map((doctorSpecialty) => {
            return {
                doctor_profile_id: doctorProfileMapping.get(doctorSpecialty["name"]),
                specialty_id: specialtyMapping.get(doctorSpecialty["specialty"])
            }
        })
   
        await trx(doctorSpecialtyTableName).insert(doctorSpecialtyData);

        const doctorOfficeHourData = doctorOfficeHour.map((doctorOfficeHour) => {
            return {
                doctor_profile_id: doctorProfileMapping.get(doctorOfficeHour["name"]),
                day: doctorOfficeHour["day"],
                period: doctorOfficeHour["period"],
                start: doctorOfficeHour["start"],
                end: doctorOfficeHour["end"]
            }
        })
        await trx(doctorOfficeHourTableName).insert(doctorOfficeHourData);

        const bookingRecordData = bookingRecord.map((bookingRecord) => {
            return {
                ticket_number: bookingRecord["ticket_number"],
                appointment_number: bookingRecord["appointment_number"],
                patient_id: patientProfileMapping.get(bookingRecord["patient_profile_id"]),
                doctor_profile_id: doctorProfileMapping.get(bookingRecord["doctor_profile_id"]),
                clinic_id: clinicMapping.get(bookingRecord["clinic_profile_id"]),
                date_of_booking: bookingRecord["date_of_booking"],
                time_of_booking: bookingRecord["time_of_booking"],
                status_id : BOOKING_STATUS.BOOKED
            }
        })
       
        await trx(bookingRecordTableName).insert(bookingRecordData);

        const diagnosisData = diagnosis.map((diagnosis) => {
            return {
                clinic_id: clinicMapping.get(diagnosis["clinic"]),
                patient_id: patientProfileMapping.get(diagnosis["patient_name"]),
                doctor_profile_id: doctorProfileMapping.get(diagnosis["doctor_name"]),
                report_date: diagnosis["report_date"],
                symptoms: diagnosis["symptoms"],
                diagnosis : diagnosis["diagnosis"],
                medication: diagnosis["medication"],
                remark: diagnosis["remark"]
            }
        })

        await trx(diagnosisReportTableName).insert(diagnosisData);

        const ticketData = ticket.map((ticket) => {
            return {
                ticket_number: ticket["ticket_number"],
                patient_id: patientProfileMapping.get(ticket["patient_profile_id"]),
                clinic_id: clinicMapping.get(ticket["clinic_profile_id"]),
                status_id : TICKET_STATUS.BOOKED
            }
        })

        await trx(ticketTableName).insert(ticketData);

        const bookMarkDoctorData = bookMarkDoctor.map((bookMarkDoctor) => {
            return {
                patient_id: patientProfileMapping.get(bookMarkDoctor["patient_name"]),
                doctor_profile_id: doctorProfileMapping.get(bookMarkDoctor["doctor_name"]),
            }
        })

        await trx(patientBookmarkTableName).insert(bookMarkDoctorData);
        const commentData = comment.map((comment) => {
            return {
                comment: comment["comment"],
                patient_id: patientProfileMapping.get(comment["patient_name"]),
                doctor_profile_id: doctorProfileMapping.get(comment["doctor_name"]),
            }
        })

        await trx(commentTableName).insert(commentData);
        await trx.commit();
    } catch (err) {
        console.error(err.message);
        await trx.rollback();
    } finally {
        await knex.destroy();
    }

};
