window.onload = async () => {
    loadAppointmentInfo();
    chooseDate();
    initSearch();
    showAll();
    loadDoctorInfo();
    loadUserinfo();
    // changeStatus();

    // Socket IO
    const clinicID = await userInfo();
    const socket = io.connect();

    // Socket IO
    socket.on(`${clinicID}_updateNewAppointment`, (update) => {
        console.log('Socket IO')
        console.log(update)
        dateformat = moment.tz(update.date_of_booking, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")
        // console.log(update.patient_name)
        // let appointment = update.appointment[0]
        document.querySelector('tbody').innerHTML += `
        <tr class="booking" data-ticket-id="${update.appointment_number}">
        <td scope="row">${update.patient_name}</td>
        <td>${update.appointment_number}</td>
        <td>${finalDate}</td>
        <td>${update.time_of_booking}</td>
        <td>${update.doctor_name}</td>
        <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${update.patient_id}'">OPEN FILE</button></td>
        <td>

        <div class="dropdown ticket-status">
            <button class="btn btn-secondary dropdown-toggle" type="button"
                id="dropdownMenuButton1" data-bs-toggle="dropdown"
                aria-expanded="false" data-ticket-status="${update.appointment_number}">
                ${bookingStatusIdToStatus(update.status_id)}
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li><a class="dropdown-item" onclick="updateAppointment(${update.appointment_number}, 2)">ARRIVED</a></li>
            <li><a class="dropdown-item" onclick="updateAppointment(${update.appointment_number}, 3)">SEEING DOCTOR</a></li>
            <li><a class="dropdown-item" onclick="updateAppointment(${update.appointment_number}, 4)">COMPLETE</a></li>
            <li><a class="dropdown-item" onclick="updateAppointment(${update.appointment_number}, 5)">CANCELLED</a></li>
            <li><a class="dropdown-item" onclick="updateAppointment(${update.appointment_number}, 6)">NO SHOW</a></li>
            </ul>
        </div>

        </td>
        </tr>
        `;
        // Update Counter
        let count = document.getElementById('total-appointment-number').innerHTML
        // console.log(count)
        let countPlus = parseInt(count)
        countPlus += 1;
        // console.log(countPlus)
        document.getElementById('total-appointment-number').innerHTML = countPlus;

    });
};

async function loadAppointmentInfo() {
    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")
    // console.log(clinicID)

    const res = await fetch(`/api/v1/appointmentClinic`);

    if (res.status === 400) {
        alert("Clinic Does Not Exist");
        window.location = '/clinic/clinicHomePage.html';
        return;
    }

    const data = await res.json();
    console.log(data.appointmentInfoforClinic)

    let info = ``;
    for (let d of data.appointmentInfoforClinic) {

        dateformat = moment.tz(d.date_of_booking, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")

        // console.log(d)
        info += `
        <tr class="booking" data-ticket-id="${d.appointment_number}">
        <td scope="row">${d.patient_name}</td>
        <td>${d.appointment_number}</td>
        <td>${finalDate}</td>
        <td>${d.time_of_booking}</td>
        <td>${d.doctor_name}</td>
        <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${d.patient_id}'">OPEN FILE</button></td>
        <td>

        <div class="dropdown ticket-status">
            <button class="btn btn-secondary dropdown-toggle" type="button"
                id="dropdownMenuButton1" data-bs-toggle="dropdown"
                aria-expanded="false" data-ticket-status="${d.appointment_number}">
                ${d.status_name}
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 2)">ARRIVED</a></li>
                <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 3)">SEEING DOCTOR</a></li>
                <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 4)">COMPLETE</a></li>
                <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 5)">CANCELLED</a></li>
                <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 6)">NO SHOW</a></li>
            </ul>
        </div>

        </td>
        </tr>
        `
    }

    // let status = data.map(d => d['status'])
    // console.log(status)

    document.querySelector('tbody').innerHTML = info;

    // count appointment numbers
    document.getElementById('total-appointment-number').innerHTML = data.appointmentInfoforClinic.length
}


async function chooseDate() {

    // console.log('pick a date')
    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")
    // console.log(clinicID)

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    // console.log(data)

    document.getElementById('date').addEventListener('change', function () {
        var input = this.value;
        // var dateEntered = new Date(input);
        console.log(input)

        let info = ``;
        let count = 0;
        for (let da of data.appointmentInfoforClinic) {
            // console.log(da)
            dateformat = moment.tz(da.date_of_booking, "Asia/Hong_Kong");
            finalDate = dateformat.format("YYYY-MM-DD")
            console.log(`for matching: ${finalDate}`)

            if (finalDate == input) {
                info += `
                    <tr class="booking" data-ticket-id="${da.appointment_number}">
                    <td scope="row">${da.patient_name}</td>
                    <td>${da.appointment_number}</td>
                    <td>${finalDate}</td>
                    <td>${da.time_of_booking}</td>
                    <td>${da.doctor_name}</td>
                    <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${da.patient_id}'">OPEN FILE</button></td>
                    <td>
            
                    <div class="dropdown ticket-status">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenuButton1" data-bs-toggle="dropdown"
                            aria-expanded="false" data-ticket-status="${da.appointment_number}">
                            ${da.status_name}
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" onclick="updateAppointment(${da.appointment_number}, 2)">ARRIVED</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${da.appointment_number}, 3)">SEEING DOCTOR</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${da.appointment_number}, 4)">COMPLETE</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${da.appointment_number}, 5)">CANCELLED</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${da.appointment_number}, 6)">NO SHOW</a></li>
                        </ul>
                    </div>
            
                    </td>
                    </tr>
                    `
                count += 1;
                document.querySelector('tbody').innerHTML = info;
                document.getElementById('total-appointment-number').innerHTML = count;
                return;
            } else {
                // console.log('shit')
                document.querySelector('tbody').innerHTML = ``;
                document.getElementById('total-appointment-number').innerHTML = count;
            }

        }

    });
}

async function initSearch() {

    const form = document.querySelector("#search-form");
    form.addEventListener("submit", async function (event) {

        event.preventDefault();

        const formObject = {
            keywords: form.keywords.value
        };

        const res = await fetch('/api/v1/searchPatient', {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject),
        })

        const data = await res.json();
        console.log(data.resultKeywords)
        let resultKey = data.resultKeywords
        console.log(resultKey)
        // let result = resultKey.map(d => d['patient_name'])

        let info = ``;
        let count = 0;

        for (let daa of resultKey) {
            console.log(daa)
            dateformat = moment.tz(daa.date_of_booking, "Asia/Hong_Kong");
            finalDate = dateformat.format("YYYY-MM-DD")

            if (resultKey.map(d => d['patient_name'])) {
                
                // console.log('yes')
                
                info += `
                <tr class="booking" data-ticket-id="${daa.appointment_number}">
                <td scope="row">${daa.patient_name}</td>
                <td>${daa.appointment_number}</td>
                <td>${finalDate}</td>
                <td>${daa.time_of_booking}</td>
                <td>${daa.doctor_name}</td>
                <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${daa.patient_id}'">OPEN FILE</button></td>
                    <td>
            
                    <div class="dropdown ticket-status">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                            id="dropdownMenuButton1" data-bs-toggle="dropdown"
                            aria-expanded="false" data-ticket-status="${daa.appointment_number}">
                            ${daa.status}
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" onclick="updateAppointment(${daa.appointment_number}, 2)">ARRIVED</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${daa.appointment_number}, 3)">SEEING DOCTOR</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${daa.appointment_number}, 4)">COMPLETE</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${daa.appointment_number}, 5)">CANCELLED</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${daa.appointment_number}, 6)">NO SHOW</a></li>
                        </ul>
                    </div>
            
                    </td>
                </tr>
                `
                count += 1;
                document.querySelector('tbody').innerHTML = info;
                // count appointment numbers
                document.getElementById('total-appointment-number').innerHTML = count;
            }
            else {
                document.querySelector('tbody').innerHTML = ``;
                // window.location.href=`/clinicAppointment.html?id=${clinicID}`
                return;
            }
        }

    })

}

function showAll() {

    document.querySelector('.show-all').addEventListener('click', function () {
        // console.log('Show All!')
        loadAppointmentInfo();
    })
}

async function loadDoctorInfo() {

    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    // console.log(data.appointmentInfoforClinic[1].doctor_name)
    // console.log('doctor')

    // let info = `
    // <div>
    // ${data.appointmentInfoforClinic[0].doctor_name}<br>
    // <div class="clinic-name"
    // >${data.appointmentInfoforClinic[0].clinic_name}</div>
    // </div>
    // `

    // document.querySelector('.doctor-info').innerHTML = info;

    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;
}

function changeStatus() {

    let select = document.querySelector('#lang');
    let result = document.querySelector('#result');
    select.addEventListener('change', function () {
        // result.textContent = this.value;
        console.log('yes')
    });

}

async function userInfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    clinicID = data.userInfo.clinic_id

    return clinicID

}

async function updateAppointment(appointmentID, statusID) {

    console.log(appointmentID)

    const res = await fetch(`/api/v1/updateAppointmentStatus/${appointmentID}/${statusID}`, {
        method: "PUT"
    })

    const data = await res.json();
    console.log("update")

    if (res.status === 200 && data.message === "success") {

        document.querySelector(`[data-ticket-status="${appointmentID}"]`).innerHTML = `${bookingStatusIdToStatus(statusID)}`


        // if (statusID === 4 || statusID === 5 || statusID === 6) {
        //     document.querySelector(`[data-ticket-id="${appointmentID}"]`).style.display = "none";
        // }

    } else {
        alert(data.message)
    }

}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}

// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}
