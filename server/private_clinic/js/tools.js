function statusIdToStatus(id) {
    const statusIdToStatus = {
        1: "BOOKED",
        2: "SEEING DOCTOR",
        3: "COMPLETED",
        4: "NO SHOW",
    }

    return statusIdToStatus[id];
}

function bookingStatusIdToStatus(id) {
    const statusIdToStatus = {
        1: "BOOKED",
        2: "ARRIVED",
        3: "SEEING DOCTOR",
        4: "COMPLETED",
        5: "CANCELLED",
        6: "NO SHOW"
    }

    return statusIdToStatus[id];
}
