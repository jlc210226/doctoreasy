window.onload = () => {
    getMedicalReport();
    loadDoctorInfo();
    loadUserinfo();
}

async function getMedicalReport() {
    const urlParams = new URLSearchParams(window.location.search);
    const medicalReportId = urlParams.get("medicalReportId")
    console.log(medicalReportId)
    const res = await fetch(`/api/v1/getMedicalReport/${medicalReportId}/`)
    const data = await res.json();
    console.log(data)
    if (data.medicalReport) {
        dateformat = moment.tz(data.medicalReport.report_date, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")
        let medicalInfoStr = `  <p class="title">Medical Report</p> <div class="medical-record">
        Patient Name：<div class="grey-container patient-name">${data.medicalReport.patient_name}</div>
        Patient gender：<div class="grey-container patient-gender">${data.medicalReport.gender}</div>
        Date <div class="grey-container date">${finalDate}</div>
        Doctor： <div class="grey-container doctor">${data.medicalReport.doctor_name}</div>
        Symptoms：<div class="grey-container symptoms">${data.medicalReport.symptoms}</div>
        Diagnosis：<div class="grey-container diagnosis">${data.medicalReport.diagnosis}</div>
        Remarks： <div class="grey-container remarks">${data.medicalReport.remarks}</div>
        Medications： <div class="grey-container medication">${data.medicalReport.medication}</div>
    </div>`
    console.log(medicalInfoStr)
            document.querySelector(".medical-report").innerHTML = medicalInfoStr
    } else {
        console.log(data.message)
    }

}

// Load Doctor Info
async function loadDoctorInfo() {

    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    console.log(data)
    console.log('doctor')

    // let info = `
    // <div>
    // ${data.appointmentInfoforClinic[0].doctor_name}<br>
    // <div class="clinic-name"
    // >${data.appointmentInfoforClinic[0].clinic_name}</div>
    // </div>
    // `

    // document.querySelector('.clinic-info').innerHTML = info;

    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;

}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}
   

// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}
