

async function getPatientProfile() {

    const urlParams = new URLSearchParams(window.location.search);
    const patientID = urlParams.get("patientID")
    console.log(patientID)
    const res = await fetch(`/api/v1/clinicpatientProfile/${patientID}`)
    const data = await res.json();
    console.log(data)
    if (data.profile.name) {
        dateformat = moment.tz(data.profile.date_of_birth, "Asia/Hong_Kong");
        finalDate = dateformat.format("YYYY-MM-DD")
        let profileInfoStr = `
        <div class="detail-profile">
        <p class="title">Patient Profile</p>
        Name: <div class="name">${data.profile.name}</div>
        Date of Birth: <div class="birthday">${finalDate}</div>
        Address: <div class="address">${data.profile.address}</div>
        Gender: <div class="gender">${data.profile.gender}</div>
        HKID: <div class="hkidNo">${data.profile.id_number}</div>
        Phone: <div class="phone">${data.profile.phone}</div>
        Email: <div class="Email">${data.profile.email}</div>
    </div>`
        const profileContainer = document.querySelector(".patient-profile")
        profileContainer.innerHTML = profileInfoStr
    } else {
        console.log(data.message)
    }
}

async function getAllMedicalRecord() {
    const urlParams = new URLSearchParams(window.location.search);
    const patientID = urlParams.get("patientID")
    const res = await fetch(`/api/v1/getAllPatientMedicalRecord/${patientID}`)
    const data = await res.json();
    console.log(data)
    if (data.medicalReports.length > 0) {
        let medicalInfoStr = `<p class="title">Medical Records</p>
        <button onclick="window.location.href='createMedicalReport.html?patientID=${patientID}'" class="add-report" id="addReportAdmin">Add Report</button>`
        for (let singleData of data.medicalReports) {
            dateformat = moment.tz(singleData.report_date, "Asia/Hong_Kong");
            finalDate = dateformat.format("YYYY-MM-DD")
            medicalInfoStr += ` <div onclick="window.location.href='/clinic/clinicViewMedicalReport.html?medicalReportId=${singleData.id}'" class="medical-record">
            Date： <span>${finalDate}</span></br>
             Diagnosis：<span>${singleData.diagnosis}</span></br>
            Doctor：<span>${singleData.name}</span>
         </div>`
        }
        console.log(medicalInfoStr)
        document.querySelector(".all-records").innerHTML = medicalInfoStr
    } else if (data.medicalReports.length == 0) {
        document.querySelector(".all-records").innerHTML = `<p class="title">Medical Records</p>
        <button onclick="window.location.href='createMedicalReport.html?patientID=${patientID}'" class="add-report" id="addReportAdmin">Add Report</button>`
    }
    else {
        console.log(data.message)
    }
}

async function checkRole() {
    const res = await fetch('/api/v1/getUserInfo');
    const data = await res.json();
    role = data.userInfo.role
    if (role !== "doctor") {
        console.log('admin')
        document.querySelector("#addReportAdmin").style.display = "none"
    }
    return
}

// Load Doctor Info
async function loadDoctorInfo() {

    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    console.log(data)
    console.log('doctor')

    // let info = `
    // <div>
    // ${data.appointmentInfoforClinic[0].doctor_name}<br>
    // <div class="clinic-name"
    // >${data.appointmentInfoforClinic[0].clinic_name}</div>
    // </div>
    // `

    // document.querySelector('.clinic-info').innerHTML = info;

    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;

}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}

// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}

window.onload = async () => {
    getPatientProfile();
    await getAllMedicalRecord();
    checkRole();
    loadDoctorInfo();
    loadUserinfo();
}