

async function getPatientInfo() {
    dateformat = moment.tz(moment().toDate(), "Asia/Hong_Kong");
    finalDate = dateformat.format("YYYY-MM-DD")
    const urlParams = new URLSearchParams(window.location.search);
    const patientID = urlParams.get("patientID")
    console.log(patientID)
    const res = await fetch(`/api/v1/doctorGetPatientProfile/${patientID}`)
    const data = await res.json();
    console.log(data)
    if (data.doctor_name) {
        const inputForm = document.getElementById("createReport")
        inputForm.patientName.value = data.patientInfo[0].name
        inputForm.patientGender.value = data.patientInfo[0].gender
        inputForm.doctorName.value = data.doctor_name
        inputForm.report_date.value = finalDate
    } else {
        console.log(data.message)
    }
}

async function createMedicalReport() {
    const inputForm = document.getElementById("createReport")
    const urlParams = new URLSearchParams(window.location.search);
    const patientID = urlParams.get("patientID")
    inputForm.addEventListener("submit", async function (event) {
        event.preventDefault();
        const formObject = {};
        formObject.patient_name = inputForm.patientName.value;
        formObject.gender = inputForm.patientGender.value;
        formObject.doctor_name = inputForm.doctorName.value;
        formObject.symptoms = inputForm.symptoms.value;
        formObject.report_date = inputForm.report_date.value;
        formObject.diagnosis = inputForm.diagnosis.value;
        formObject.remarks = inputForm.remarks.value;
        formObject.medications = inputForm.medications.value;
        const res = await fetch(`/api/v1/createReport/${patientID}`, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })
        const data = await res.json()
        if(res.status === 200) {
            alert("Created new medical report.")
            window.location = "/clinic/clinicHomePage.html"
        }else {
            console.log(res.message)
        }

    })
}

// Load Doctor Info
async function loadDoctorInfo() {

    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    console.log(data)
    console.log('doctor')

    // let info = `
    // <div>
    // ${data.appointmentInfoforClinic[0].doctor_name}<br>
    // <div class="clinic-name"
    // >${data.appointmentInfoforClinic[0].clinic_name}</div>
    // </div>
    // `

    // document.querySelector('.clinic-info').innerHTML = info;

    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;

}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}

// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}

window.onload = () => {
    getPatientInfo()
    createMedicalReport()
    loadDoctorInfo();
}
