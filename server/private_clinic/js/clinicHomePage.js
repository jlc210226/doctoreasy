window.onload = async () => {
    getTickets();
    loadAppointmentInfo();
    loadClinicInfo();
    loadUserinfo()

    const clinicID = await userInfo();

    const socket = io.connect();

    // update now serving ticket number
    socket.on(`${clinicID}_updateServingTicket`, (update) => {
        console.log(update);

        let ticketID = update.ticketID;
        let statusID = update.statusID;

        if ( update.appointmentID ){
            let appointmentID = update.appointmentID;
            let appointmentDiv = document.querySelector(`[data-ticket-id="${appointmentID}"]`);
            let appointmentStatusDiv = document.querySelector(`[data-ticket-status="${appointmentID}"]`);
            
            //handle bookings
            if (appointmentDiv) {
                if (statusID >= 4){
                    appointmentDiv.style.display = "none";
                } else {
                    appointmentStatusDiv.innerHTML = bookingStatusIdToStatus(statusID);
                }
            } else if (!appointmentDiv){ //if the booking patient arrived
                let appointment = update.appointment
                
                let info = `
                <tr class="booking" data-ticket-id="${appointment.appointment_number}">
                <td scope="row">${appointment.patient_name}</td>
                <td>${appointment.appointment_number}</td>
                <td>APPOINTMENT</td>
                <td>${appointment.doctor_name}</td>
                <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${appointment.patient_id}'">OPEN FILE</button></td>
                <td>
        
                <div class="dropdown ticket-status">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton1" data-bs-toggle="dropdown"
                        aria-expanded="false" data-ticket-status="${appointment.appointment_number}">
                        ${appointment.status}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" onclick="updateAppointment(${appointment.appointment_number}, 3)">SEEING DOCTOR</a></li>
                        <li><a class="dropdown-item" onclick="updateAppointment(${appointment.appointment_number}, 4)">COMPLETE</a></li>
                    </ul>
                </div>
        
                </td>
                </tr>
                `

                document.querySelector("#ticket-list").innerHTML += info;
            }

        } else {
            //handle tickets
            if (statusID === 3 || statusID === 4) {
                document.querySelector(`[data-ticket-id="${ticketID}"]`).style.display = "none";
            }else{
                if (document.querySelector(`[data-ticket-status="${ticketID}"]`)){
                    document.querySelector(`[data-ticket-status="${ticketID}"]`).innerHTML = statusIdToStatus(statusID);
                }
            }
        }
    });

    //new ticket
    socket.on(`${clinicID}_updateNextTicket`, (update) => {

        let ticket = update.ticket[0]

        console.log(ticket)

        document.querySelector("#ticket-list").innerHTML += `
        <tr class="ticket" data-ticket-id="${ticket.ticket_id}">
            <td scope="row">${ticket.name}</td>
            <td>${ticket.ticket_number}</td>
            <td>Ticket</td>
            <td>TBC</td>
            <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${ticket.patient_id}'">OPEN FILE</button></td>
            <td>

                <div class="dropdown ticket-status">
                    <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton1" data-bs-toggle="dropdown"
                        aria-expanded="false" data-ticket-status="${ticket.ticket_id}">
                        ${ticket.status}
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 2)">SEEING DOCTOR</a></li>
                        <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 3)">COMPLETE</a></li>
                        <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 4)">NO SHOW</a></li>
                    </ul>
                </div>

            </td>
        </tr>
        `
    });

    clinicGetNextTicketNumber();

    // clinic set ticket number
    socket.on(`${clinicID}_clinicUpdateNextTicket`, (update) => {
        document.querySelector('#next-ticket-number').innerHTML = update.nextTicket;
    });

}

async function userInfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    clinicID = data.userInfo.clinic_id

    return clinicID

}

async function updateTicket(ticketID, statusID) {

    const res = await fetch(`/api/v1/updateTicketStatus/${ticketID}/${statusID}`, {
        method: "PUT"
    })

    const data = await res.json();

    if (res.status === 200 && data.message === "success") {

        // document.querySelector(`[data-ticket-id="${ticketID}"]`)
        document.querySelector(`[data-ticket-status="${ticketID}"]`).innerHTML = `${statusIdToStatus(statusID)}`


        if (statusID === 4 || statusID === 3) {
            document.querySelector(`[data-ticket-id="${ticketID}"]`).style.display = "none";
        }

    } else {
        alert(data.message)
    }

}

async function getTickets() {
    const res = await fetch('/api/v1/clinicGetTicket');

    const tickets = await res.json();

    if (res.status !== 200) {
        alert(data.message);
        return
    }

    let ticketsStr = ``;

    for (const ticket of tickets) {
        console.log(ticket)
        ticketsStr += `
        <tr class="ticket" data-ticket-id="${ticket.ticket_id}">
            <td scope="row">${ticket.name}</td>
            <td>${ticket.ticket_number}</td>
            <td>Ticket</td>
            <td>TBC</td>
            <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${ticket.patient_id}'">OPEN FILE</button></td>
            <td>

            <div class="dropdown ticket-status">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                    id="dropdownMenuButton1" data-bs-toggle="dropdown"
                    aria-expanded="false" data-ticket-status="${ticket.ticket_id}">
                    ${statusIdToStatus(ticket.status_id)}
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 2)">SEEING DOCTOR</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 3)">COMPLETE</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 4)">NO SHOW</a></li>
                </ul>
            </div>

            </td>
        </tr>
        `


    }

    document.querySelector("#ticket-list").innerHTML += ticketsStr;

}

async function clinicGetNextTicketNumber() {
    const res = await fetch(`/api/v1/clinicGetNextTicketNumber`)
    const nextTicket = await res.json();

    document.querySelector("#next-ticket-number").innerHTML = `${nextTicket}`;

}

// Fetch Arrived Appointment
async function loadAppointmentInfo() {

    const res = await fetch(`/api/v1/appointmentClinic`);

    if (res.status === 400) {
        alert("Clinic Does Not Exist");
        window.location = '/clinic/clinicHomePage.html';
        return;
    }

    const data = await res.json();
    // console.log(data.appointmentInfoforClinic)


    let info = ``;
    for (let d of data.appointmentInfoforClinic) {

        if (d.status === 2 || d.status === 3) {
            info += `
            <tr class="booking" data-ticket-id="${d.appointment_number}">
            <td scope="row">${d.patient_name}</td>
            <td>${d.appointment_number}</td>
            <td>APPOINTMENT</td>
            <td>${d.doctor_name}</td>
            <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${d.patient_id}'">OPEN FILE</button></td>
            <td>
    
            <div class="dropdown ticket-status">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                    id="dropdownMenuButton1" data-bs-toggle="dropdown"
                    aria-expanded="false" data-ticket-status="${d.appointment_number}">
                    ${d.status_name}
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 3)">SEEING DOCTOR</a></li>
                    <li><a class="dropdown-item" onclick="updateAppointment(${d.appointment_number}, 4)">COMPLETE</a></li>
                </ul>
            </div>
    
            </td>
            </tr>
            `
        }

    }

    document.querySelector("#ticket-list").innerHTML += info;

}

// Appointment Status
function statusIdToStatusAppointment(id) {
    const statusIdToStatusAppointment = {
        1: "BOOKED",
        2: "ARRIVED",
        3: "SEEING DOCTOR",
        4: "COMPLETED",
        5: "CANCELLED",
        6: "NO SHOW",
    }

    return statusIdToStatusAppointment[id];
}

// Update Appointment Status
async function updateAppointment(appointmentID, statusID) {

    const res = await fetch(`/api/v1/updateAppointmentStatus/${appointmentID}/${statusID}`, {
        method: "PUT"
    })

    const data = await res.json();
    console.log("update")

    if (res.status === 200 && data.message === "success") {

        document.querySelector(`[data-ticket-status="${appointmentID}"]`).innerHTML = `${bookingStatusIdToStatus(statusID)}`

        // if (statusID === 3) {
        //     document.querySelector(`[data-ticket-id="${appointmentID}"]`).classList.toggle("seeing");
        // }

        if (statusID === 4) {
            document.querySelector(`[data-ticket-id="${appointmentID}"]`).style.display = "none";
        }

    } else {
        alert(data.message)
    }

}

// Load Doctor Info
async function loadClinicInfo() {

    // const urlParams = new URLSearchParams(window.location.search);
    // const clinicID = urlParams.get("id")

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
    console.log(data)
    console.log('doctor')

    // let info = `
    // <div>
    // ${data.appointmentInfoforClinic[0].doctor_name}<br>
    // <div class="clinic-name"
    // >${data.appointmentInfoforClinic[0].clinic_name}</div>
    // </div>
    // `

    // let info = `
    // <div class="clinic-name">${data.appointmentInfoforClinic[0].clinic_name}</div>
    // `

    // document.querySelector('.clinic-info').innerHTML = info;

    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;
}

// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}

