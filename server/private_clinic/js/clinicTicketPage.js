window.onload = async function () {
    loadAndDisplayTickets()
    clinicGetCurrentServingTicket()
    checkandDisplayQueueActiveForClinic()
    clinicSetTicketNumber()
    clinicGetNextTicketNumber()
    loadDoctorInfo();
    loadUserinfo() 

    const clinicID = await userInfo();

    const socket = io.connect();
    console.log("init connect");

    // update next ticket number + insert new ticket into ticket queue
    socket.on(`${clinicID}_updateNextTicket`, (update) => {

        const ticketDate = moment.tz(update.ticket[0].ticket_created_at, "Asia/Hong_Kong").format("YYYY-MM-DD h:mm:ss a");

        document.querySelector('.ticket-board').innerHTML = update.nextTicket;

        let newTicketString =
            `
        <tr class="ticket">
        <td scope="row">${update.ticket[0].name}</td>
        <td>${update.ticket[0].ticket_number}</td>
        <td>${ticketDate}</td>
      
        <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${update.ticket[0].user_patient_id}'">OPEN FILE</button></td>
        <td>

            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                    id="dropdownMenuButton1" data-bs-toggle="dropdown"
                    aria-expanded="false" data-ticket-status="${update.ticket[0].ticket_id}">
                    ${update.ticket[0].status}
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" onclick="updateTicket(${update.ticket[0].ticket_id}, 1)">BOOKED</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${update.ticket[0].ticket_id}, 2)">SEEING DOCTOR</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${update.ticket[0].ticket_id}, 3)">COMPLETE</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${update.ticket[0].ticket_id}, 4)"> NO SHOW </a> </li>
                </ul>
            </div>

        </td>
        </tr>
        `

        document.querySelector('.table-body').innerHTML += newTicketString;

    });

    // update now serving ticket number
    socket.on(`${clinicID}_updateServingTicket`, (update) => {

        let ticketID = update.ticketID;
        let statusID = update.statusID;
        let ticketNumber = update.ticketNumber;

        if (!update.appointmentID) {
            //handle tickets
            document.querySelector(`[data-ticket-status="${ticketID}"]`).innerHTML = statusIdToStatus(statusID);   
            return;
        }

        if (statusID === 2) {
            document.querySelector('.now-serving').innerHTML = ticketNumber;
            return;
        }
   
    });

}

async function startTheQueue() {

    const res = await fetch(`/api/v1/startTicketQueue`);
    const result = await res.json()

    if (res.status === 200 && result.message === "Queue started") {
        alert("Ticket Queue started.");
        return;
    }

}

async function stopTheQueue() {

    const res = await fetch(`/api/v1/stopTicketQueue`);
    const result = await res.json()

    if (res.status === 200 && result.message === "Queue stopped") {
        alert("Ticket Queue stopped.");
        return;
    }

}

async function loadAndDisplayTickets() {

    const res = await fetch(`/api/v1/clinicGetTodayTickets`)
    const result = await res.json();

    let ticketStr = ``
    for (ticket of result) {

        const ticketDate = moment.tz(ticket.ticket_created_at, "Asia/Hong_Kong").format("YYYY-MM-DD h:mm:ss a");

        ticketStr += `
        <tr class="ticket">
        <td scope="row">${ticket.name}</td>
        <td>${ticket.ticket_number}</td>
        <td>${ticketDate}</td>
        
        <td><button onclick="window.location.href='/clinic/clinicpatientProfile.html?patientID=${ticket.patient_id}'">OPEN FILE</button></td>
        <td>

            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                    id="dropdownMenuButton1" data-bs-toggle="dropdown"
                    aria-expanded="false" data-ticket-status="${ticket.ticket_id}">
                    ${statusIdToStatus(ticket.status_id)}
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 1)">BOOKED</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 2)">SEEING DOCTOR</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 3)">COMPLETE</a></li>
                    <li><a class="dropdown-item" onclick="updateTicket(${ticket.ticket_id}, 4)"> NO SHOW </a> </li>
                </ul>
            </div>

        </td>
        </tr>
        `
    }

    document.querySelector('.table-body').innerHTML += ticketStr;

}

async function checkandDisplayQueueActiveForClinic() {

    const res = await fetch(`/api/v1/checkQueueActiveForClinic`);
    const result = await res.json();
    const status = result[0].queue_active

    if (status === false) {
        document.getElementById("queue-switch").checked = false;
        document.querySelector('.form-check-label').innerHTML = 'Queue OFF'
    } else {
        document.getElementById("queue-switch").checked = true;
        document.querySelector('.form-check-label').innerHTML = 'Queue ON'
    }

}

async function clinicSetTicketNumber() {

    const setTicketNumberForm = document.querySelector('.set-ticket-number-form');

    setTicketNumberForm.addEventListener("submit", async function (event) {
        event.preventDefault();
        const formObject = {}
        formObject["ticketNumber"] = setTicketNumberForm.setTicket.value;

        const res = await fetch("/api/v1/clinicSetTicketNumber", {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })

        const result = await res.json();

        if (res.status === 200 && result.message == "Ticket Number Set") {
            document.querySelector(".ticket-board").innerHTML = `${result.ticketNumberSet}`;
            return;
        }

    })
}

async function clinicGetNextTicketNumber() {
    const res = await fetch(`/api/v1/clinicGetNextTicketNumber`)
    const nextTicket = await res.json();

    document.querySelector(".ticket-board").innerHTML = `${nextTicket}`;

}

async function userInfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    clinicID = data.userInfo.clinic_id

    return clinicID

}

async function clinicGetCurrentServingTicket() {

    // 1. Check if any ticket seeing doctor 
    res = await fetch(`/api/v1/clinicGetCurrentServingTicket`);
    servingNumbers = await res.json();

    if (servingNumbers.length > 0) {

        let maxTicketNum = findMaxNumber(servingNumbers);

        document.querySelector('.now-serving-ticket-number').innerHTML = maxTicketNum;

        return;
    }

    // 2. Check if any ticket last served 
    res = await fetch(`/api/v1/clinicGetLastServedTicket`);
    servingNumbers = await res.json();

    if (servingNumbers.length > 0) {

        let maxTicketNum = findMaxNumber(servingNumbers);

        document.querySelector('.now-serving-ticket-number').innerHTML = maxTicketNum;

        return;
    }

}

function findMaxNumber(servingNumbers) {
    const maxNumberTicket = servingNumbers.reduce(function (previous, current) {
        if (current.ticket_number > previous.ticket_number) {
            return current
        } else {
            return previous
        }
    }, { 'ticket_number': 1 }).ticket_number

    return maxNumberTicket;
}

// Queue ON OFF Switch
document.querySelector('.form-check-input').addEventListener('click', (event) => {

    if (document.querySelector('input[type=checkbox][name=queue]:checked')) {
        document.querySelector('.form-check-label').innerHTML = 'Queue ON'
        startTheQueue();
        return;
    } else {
        document.querySelector('.form-check-label').innerHTML = 'Queue OFF'
        stopTheQueue();
        return;
    }

})

function statusIdToStatus(id) {
    const statusIdToStatus = {
        1: "BOOKED",
        2: "SEEING DOCTOR",
        3: "COMPLETED",
        4: "NO SHOW",
    }

    return statusIdToStatus[id];
}

// Appointment Status
function statusIdToStatusAppointment(id) {
    const statusIdToStatusAppointment = {
        1: "BOOKED",
        2: "ARRIVED",
        3: "SEEING DOCTOR",
        4: "COMPLETED",
        5: "CANCELLED",
        6: "NO SHOW",
    }

    return statusIdToStatusAppointment[id];
}

async function updateTicket(ticketID, statusID) {

    const res = await fetch(`/api/v1/updateTicketStatus/${ticketID}/${statusID}`, {
        method: "PUT"
    })

    const data = await res.json();

    if (res.status === 200 && data.message === "success") {

        document.querySelector(`[data-ticket-status="${ticketID}"]`).innerHTML = `${statusIdToStatus(statusID)}`

    } else {
        alert(data.message)
    }

}

// Update Appointment Status
async function updateAppointment(appointmentID, statusID) {

    const res = await fetch(`/api/v1/updateAppointmentStatus/${appointmentID}/${statusID}`, {
        method: "PUT"
    })

    const data = await res.json();
    console.log("update")

    if (res.status === 200 && data.message === "success") {

        document.querySelector(`[data-ticket-status="${appointmentID}"]`).innerHTML = `${bookingStatusIdToStatus(statusID)}`


        if (statusID === 4) {
            document.querySelector(`[data-ticket-id="${appointmentID}"]`).style.display = "none";
        }

    } else {
        alert(data.message)
    }

}

// Load Doctor Info
async function loadDoctorInfo() {

    const res = await fetch(`/api/v1/appointmentClinic`);
    const data = await res.json();
 
    document.querySelector('.clinic-name').innerHTML = data.appointmentInfoforClinic[0].clinic_name;

}

async function loadUserinfo() {
    const res = await fetch('/api/v1/getUserInfo');

    const data = await res.json();

    const username = data.userInfo.username

    document.querySelector('.user-name').innerHTML = username;
}


// Logout
async function clinicLogout() {
    const res = await fetch("/api/v1/logout");
    const data = await res.json()
    if(res.status === 200) {
        window.location = "/clinicLogin.html"
    }else {
        console.log(data.message)
    }
}
