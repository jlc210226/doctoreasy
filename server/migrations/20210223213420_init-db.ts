import * as Knex from "knex";
import { commentTableName, patientBookmarkTableName, ticketTableName, bookingRecordTableName, ticketStatusTableName, bookingStatusTableName, doctorOfficeHourTableName, diagnosisReportTableName, doctorSpecialtyTableName, specialtyTableName, userMedicalTableName, clinicProfileTableName, districtTableName, patientProfileTableName, userPatientTableName, doctorProfileTableName } from "../utils/tables"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(userPatientTableName, (table) => {
        table.increments();
        table.string("username").notNullable().unique();
        table.string("password").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(patientProfileTableName, (table) => {
        table.increments();
        table.integer("user_patient_id").unsigned();
        table.foreign("user_patient_id").references("user_patient.id");
        table.string("name").notNullable();
        table.date("date_of_birth");
        table.text("address");
        table.string("gender");
        table.string("id_number").notNullable().unique();
        table.string("phone").unique();
        table.string("email");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(districtTableName, (table) => {
        table.increments();
        table.string("district").notNullable().unique();
    })

    await knex.schema.createTable(clinicProfileTableName, (table) => {
        table.increments();
        table.string("name").notNullable();
        table.text("address").notNullable();
        table.string("phone");
        table.integer("district_id").unsigned();
        table.foreign("district_id").references("district.id");
        table.text("opening_hours");
        table.boolean("queue_active")
        table.timestamps(false, true);
    });

    await knex.schema.createTable(userMedicalTableName, (table) => {
        table.increments();
        table.string("username").notNullable().unique();
        table.string("password").notNullable();
        table.string("role").notNullable();
        table.integer("clinic_profile_id").unsigned();
        table.foreign("clinic_profile_id").references("clinic_profile.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(doctorProfileTableName, (table) => {
        table.increments();
        table.string("name").notNullable();
        table.string("gender");
        table.string("registration_no").notNullable().unique();
        table.string("profile_pic");
        table.integer("user_medical_id").unsigned();
        table.foreign("user_medical_id").references("user_medical.id");
        table.float("rating");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(specialtyTableName, (table) => {
        table.increments();
        table.string("specialty").notNullable();
        table.timestamps(false, true);
    });

    await knex.schema.createTable(doctorSpecialtyTableName, (table) => {
        table.increments();
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
        table.integer("specialty_id").unsigned();
        table.foreign("specialty_id").references("specialty.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(diagnosisReportTableName, (table) => {
        table.increments();
        table.integer("patient_id").unsigned();
        table.foreign("patient_id").references("patient_profile.id");
        table.integer("clinic_id").unsigned();
        table.foreign("clinic_id").references("clinic_profile.id");
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
        table.date("report_date");
        table.text("symptoms");
        table.text("diagnosis");
        table.text("medication");
        table.text("remark");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(doctorOfficeHourTableName, (table) => {
        table.increments();
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
        table.integer("day")
        table.string("period");
        table.string("start");
        table.string("end");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(ticketStatusTableName, (table) => {
        table.integer("id").notNullable().unique();
        table.string("status").notNullable().unique();
    })

    await knex.schema.createTable(bookingStatusTableName, (table) => {
        table.integer("id").notNullable().unique();
        table.string("status").notNullable().unique();
    })

    await knex.schema.createTable(bookingRecordTableName, (table) => {
        table.increments();
        table.string("ticket_number");
        table.string("appointment_number");
        table.integer("patient_id").unsigned();
        table.foreign("patient_id").references("patient_profile.id");
        table.integer("clinic_id").unsigned();
        table.foreign("clinic_id").references("clinic_profile.id");
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
        table.date("date_of_booking");
        table.time("time_of_booking");
        table.integer("status_id").unsigned();
        table.foreign("status_id").references("booking_status.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(ticketTableName, (table) => {
        table.increments();
        table.string("ticket_number")
        table.integer("patient_id").unsigned();
        table.foreign("patient_id").references("patient_profile.id");
        table.integer("clinic_id").unsigned();
        table.foreign("clinic_id").references("clinic_profile.id");
        table.integer("status_id").unsigned();
        table.foreign("status_id").references("ticket_status.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(patientBookmarkTableName, (table) => {
        table.increments();
        table.integer("patient_id").unsigned();
        table.foreign("patient_id").references("patient_profile.id");
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
        table.timestamps(false, true);
    });

    await knex.schema.createTable(commentTableName, (table) => {
        table.increments();
        table.string("comment");
        table.integer("patient_id").unsigned();
        table.foreign("patient_id").references("patient_profile.id");
        table.integer("doctor_profile_id").unsigned();
        table.foreign("doctor_profile_id").references("doctor_profile.id");
    })

}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(commentTableName);
    await knex.schema.dropTable(patientBookmarkTableName);
    await knex.schema.dropTable(ticketTableName);
    await knex.schema.dropTable(bookingRecordTableName);
    await knex.schema.dropTable(ticketStatusTableName);
    await knex.schema.dropTable(bookingStatusTableName);
    await knex.schema.dropTable(doctorOfficeHourTableName);
    await knex.schema.dropTable(diagnosisReportTableName)
    await knex.schema.dropTable(doctorSpecialtyTableName);
    await knex.schema.dropTable(specialtyTableName);
    await knex.schema.dropTable(doctorProfileTableName);
    await knex.schema.dropTable(userMedicalTableName);
    await knex.schema.dropTable(clinicProfileTableName);
    await knex.schema.dropTable(districtTableName);
    await knex.schema.dropTable(patientProfileTableName);
    await knex.schema.dropTable(userPatientTableName);

}

