import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {

    if (! (await knex.schema.hasColumn('doctor_profile', 'comment_count'))) {
        await knex.schema.table('doctor_profile', function (table) {
            table.integer('comment_count');
        })
    }

}

export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasColumn('doctor_profile', 'comment_count')) {
        knex.schema.table('doctor_profile', function (table) {
            table.dropColumn('comment_count');
        })
    }
}