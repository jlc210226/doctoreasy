window.onload = () => {
    registerandCreateProfile()
}

function registerandCreateProfile() {
    const registerAndCreateForm = document.getElementById("register_createProfile")
    registerAndCreateForm.addEventListener("submit", async function (event) {
        event.preventDefault();
        const formObject = {};
        formObject["username"] = registerAndCreateForm.username.value;
        formObject["password"] = registerAndCreateForm.password.value;
        formObject["confirmpassword"] = registerAndCreateForm.confirmpassword.value;
        formObject["id_name"] = registerAndCreateForm.id_name.value;
        formObject["birthday"] = registerAndCreateForm.birthday.value;
        formObject["address"] = registerAndCreateForm.address.value;
        formObject["gender"] = registerAndCreateForm.gender.value;
        formObject["id_number"] = registerAndCreateForm.id_number.value;
        formObject["phone"] = registerAndCreateForm.phone.value;
        formObject["email"] = registerAndCreateForm.email.value;
        const checkPassword = formObject.password
        const checkConfirmPassword = formObject.confirmpassword
        console.log(checkPassword, checkConfirmPassword)
        if (checkPassword !== checkConfirmPassword) {
            alert("wrong password")
            return
        }

        console.log(formObject["id_name"])
        console.log(formObject["birthday"])
        console.log(formObject["gender"])


        const res = await fetch("/api/v1/register&createProfile", {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })
        const result = await res.json();
        if (res.status === 200) {
            window.location = "/index.html"
        } else {
            alert(result.message)
        }
    })
}

// function checkDigit(idNumber) {
//     const count = {
//         A: 10, B: 11, C: 12, D: 13, E: 14, F: 15,
//         G: 16, H: 17, I: 18, J: 19, K: 20, L: 21,
//         M: 22, N: 23, O: 24, P: 25, Q: 26, R: 27,
//         S: 28, T: 29, U: 30, V: 31, W: 32, X: 33,
//         Y: 34, Z: 35
//     }
//         let count = 36 * 9 + count[`${idNumber[i]}`]
//         for (let i = 1; i < 7; i++) {
//             let j = 7;
//             count += idNumber[i] * j;
//             j -- ;
//         }
//         let number = count % 11
//         if(number === 0) {
//             if (number === idNumber[7]) {
//                 return true
//             }
//         }else if (number === 1) {
//             if (number === idNumber[7]) {
//                 return true
//             }
//         }else {
//             if (11 - number === idNumber[7] ) {
//                 return true
//             }
//         }
// }