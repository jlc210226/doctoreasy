window.onload = () => {
    generateCalendar();
    submit();
    // loadTimeSlot();
    loadDoctorInfo();
    loginCheck();
};

let finalDate;

let loggedInbuttonSet = `
        <div class="navbar" role="group" aria-label="navbar">
        <button onclick="window.location.href='/bookmark.html'" type="button" class="btn btn-primary">
        <i class="fas fa-bookmark"></i>
        </button>
        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
        class="fas fa-home"></i>
        <button onclick="getOwnTicket()" type="button" class="btn btn-primary own-ticket">
            <i class="fas fa-ticket-alt"></i>
        </button>
        <button onclick="window.location.href='/patientProfile.html'" type="button" class="btn btn-primary">
            <i class="fas fa-address-card"></i>
        </button>
        </div>
        `
let visitorButtonSet = `
        <div class="navbar" role="group" aria-label="navbar">

        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
                class="fas fa-home"></i>

        </button>
        <button type="submit" id="login-button" onclick="window.location.href='/login.html'">登入</button>

        </div>
        `
async function loginCheck() {
    const res = await fetch('/api/v1/loginCheck');

    const data = await res.json();

    if (data.message === "LoggedIn") {
        document.querySelector(".reserved").innerHTML = loggedInbuttonSet
        // document.querySelector("#login-button").style.display = "none";
    } else {
        document.querySelector(".reserved").innerHTML = visitorButtonSet
        // document.querySelector("#login-button").style.display = "block";
    }
}

const months = [
    "一月",
    "二月",
    "三月",
    "四月",
    "五月",
    "六月",
    "七月",
    "八月",
    "九月",
    "十月",
    "十一月",
    "十二月"
];

const weekdays = ["Sun", "Mon", "Tue", "Wed", "Thur", "fri", "Sat"];
const td = '';

// 
let date = new Date();

// Current Date
function getCurrentDate(element, asString) {
    if (element) {
        if (asString) {
            return element.textContent = weekdays[date.getDay()] + ', ' + date.getDate() + "  " + months[date.getMonth()] + "  " + date.getFullYear();
        }
        return element.value = date.toISOString().substr(0, 10);
    }
    return date;
}


// Calendar
async function generateCalendar() {

    // Calendar
    const calendar = document.getElementById('calendar');
    if (calendar) {
        calendar.remove();
    }

    // Table
    const table = document.createElement("table");
    table.id = "calendar";

    // Generate Weekend
    const trHeader = document.createElement('tr');
    trHeader.className = 'weekends';
    weekdays.map(week => {
        const th = document.createElement('th');
        const w = document.createTextNode(week.substring(0, 3));
        th.appendChild(w);
        trHeader.appendChild(th);
    });

    // Header
    table.appendChild(trHeader);

    // Date
    const weekDay = new Date(
        date.getFullYear(),
        date.getMonth(),
        1
    ).getDay();

    // Date
    const lastDay = new Date(
        date.getFullYear(),
        date.getMonth() + 1,
        0
    ).getDate();

    let tr = document.createElement("tr");
    let td = '';
    let empty = '';
    let btn = document.createElement('button');
    let week = 0;

    // Count Weekday
    while (week < weekDay) {
        td = document.createElement("td");
        empty = document.createTextNode(' ');
        td.appendChild(empty);
        tr.appendChild(td);
        week++;
    }

    // Calendar Select Date
    for (let i = 1; i <= lastDay;) {
        // 
        while (week < 7) {
            td = document.createElement('td');
            let text = document.createTextNode(i);
            btn = document.createElement('button');
            btn.className = "btnDay";
            btn.setAttribute("value", date);

            btn.addEventListener('click', function () {

                // get Selected Date
                console.log('selected date:')
                selectDate = parseInt(text.nodeValue)
                btn.setAttribute("value", selectDate);
                console.log(selectDate)
                changeDate(this)

            });
            week++;


            // get Selected Date
            if (i <= lastDay) {
                i++;
                btn.appendChild(text);
                td.appendChild(btn)
            } else {
                text = document.createTextNode(' ');
                td.appendChild(text);
            }
            tr.appendChild(td);
        }
        // Generate Date
        table.appendChild(tr);

        // 
        tr = document.createElement("tr");

        // 
        week = 0;

    }



    // Adiciona a tabela a div que ela deve pertencer
    const content = document.getElementById('table');
    content.appendChild(table);
    changeActive();
    changeHeader(date);
    document.getElementById('date').textContent = date;
    getCurrentDate(document.getElementById("currentDate"), true);
    getCurrentDate(document.getElementById("date"), false);
    dateformat = moment(date, "YYYY-MM-DD");
    finalDate = dateformat.format("YYYY-MM-DD")
    // console.log(typeof (finalDate))

    let whichDay = date.getDay()

    // Generate time-slot by Date
    console.log(date.getDay())

    // Fetch doctor opening hour
    // async function loadDoctorHour() {
    //     console.log('doctorhour')
    //     const hourRes = await fetch(`api/v1/appointment/${doctorID}`);
    //     const doctorHour = await hourRes.json();
    // }

    // Generate time-slot
    const urlParams = new URLSearchParams(window.location.search);
    const doctorID = urlParams.get("id")
    const doctorTimeRes = await fetch(`api/v1/appointment/doctorTime/${doctorID}`);
    const timeSlots = await doctorTimeRes.json();

    // fetch doctor booked appointment
    const doctorAppointmentRes = await fetch(`api/v1/appointment/doctorApp/${doctorID}`);
    const timeSlotsBooked = await doctorAppointmentRes.json();
    let testTimes = timeSlotsBooked.doctorTimeBooked;
    // mapping the cannot book time-slot
    let cannotBook = testTimes.map(testTime => testTime['time_of_booking'])
    console.log(cannotBook)
    let cannotBookDate = testTimes.map(testTime => testTime['date_of_booking'])
    console.log(cannotBookDate)

    var optionDiv = document.getElementById('time-slot');

    let timeSlotss = timeSlots.timeSlot[0]
    // console.log(timeSlots.timeSlot[0])
    // console.log(timeSlots.timeSlot[0][0].start)

    // Clear time-slot before appendChild
    document.getElementById("time-select").innerText = null;

    for (let tS of timeSlotss) {

        let timeStart = tS.start
        let timeEnd = tS.end
        first2TS = timeStart.substring(0, 2)
        first2TE = timeEnd.substring(0, 2)

        v = first2TE - first2TS

        // still writing
        timeSlot = [`${timeStart}:00`]

        for (w = 1; w < v; w++) {
            timeSlot.push((parseInt(timeStart.substring(0, 2)) + w) + `:00:00`)
        }

        timeSlot.push(`${timeEnd}:00`)

        for (var timeSection of timeSlot) {

            console.log(`[picked] ${selectDate}`)
            // console.log(`[weekday] ${whichDay}`)

            if (whichDay == 0) {
                whichDay += 7;
            }

            if (whichDay == tS.day) {

                // if (cannotBook.indexOf(timeSection) <= 0) {

                var x = document.createElement("option");
                x.setAttribute("id", "timeSlot");
                x.setAttribute("class", "timeSlot");
                var t = document.createTextNode(timeSection);
                x.appendChild(t);
                document.getElementById("time-select").appendChild(x);
                // console.log(t)
                x.setAttribute("value", timeSection);

                // }

            }


        }

    }



    // var timeSlots = ['10:00:00', '11:00:00', '12:00:00', '13:00:00', '14:00:00', '15:00:00', '16:00:00']

    // // const urlParams = new URLSearchParams(window.location.search);
    // // const doctorID = urlParams.get("id")
    // // const doctorTimeRes = await fetch(`api/v1/appointment/doctorTime/${doctorID}`);
    // // const timeSlots = await doctorTimeRes.json();
    // // console.log(timeSlots)

    // var optionDiv = document.getElementById('time-slot');

    // for (var timeSlot of timeSlots) {
    //     // console.log(timeSlot)
    //     var x = document.createElement("option");

    //     x.setAttribute("id", "timeSlot");
    //     x.setAttribute("class", "timeSlot");
    //     var t = document.createTextNode(timeSlot);
    //     x.appendChild(t);
    //     document.getElementById("time-select").appendChild(x);
    //     // console.log(t)

    //     x.setAttribute("value", timeSlot);

    // }


    // Selected time-slot
    let selectTime = document.getElementById("time-select");
    selectTime.addEventListener('change', function () {
        // get Selected TimeSlot
        console.log('selected time-slot:')
        let options = selectTime.value
        console.log(options)
        // selectTime = parseInt(options.nodeValue)
        // console.log(selectTime)

        // finalSelectTime = timeSection.substring(0, 2)
        // console.log(`diu ${finalSelectTime}`)

        // // Check time-availablity and remove time slot
        // for (let testTime of testTimes) {
        //     let cannotBook = testTime['time_of_booking']
        //     console.log(cannotBook)

        //     if (options == cannotBook) {
        //         console.log('Selected time-slot is not available')
        //         // document.getElementById("timeSlot").innerText = null;
        //     }
        // }
    });


    // ----------------


    // const urlParams = new URLSearchParams(window.location.search);
    // const doctorID = urlParams.get("id")
    // console.log(doctorID)

    // async function renderClinicID() {
    //     const getData = await fetch(`api/v1/appointment/doctor/${doctorID}`);
    //     console.log(`[clinicID] ${getData}`)
    //     const clinicID = await getData.json();
    //     console.log(`[clinicID] ${clinicID}`)
    // }

    // Submit form - still writing
    // |
    // |
    // |
    // function submitDateAndTime() {



    // }

}

// submit form
function submit() {
    const subForm = document.getElementById('date-search');
    // console.log(subForm)
    subForm.addEventListener('submit', async (e) => {
        try {
            e.preventDefault();
            // console.log('loading')

            const urlParams = new URLSearchParams(window.location.search);
            const doctorID = urlParams.get("id")
            console.log(doctorID)

            // const res = await fetch(`/appointment?doctorID=${doctorID}`);
            // const docData = res.json();

            // const date = subForm.btnDay.value;
            // const time = subForm.timeSlot.value;
            const date = finalDate;
            const time = document.getElementById("time-select").value;
            // console.log(`[info-date] ${date}`)
            // console.log(`[info-time] ${time}`)
            // const appointmentNum = `${date} + ${doctorID} + ${1}`;

            const formData = { date, time }
            console.log("1 : " + formData)

            const res = await fetch(`/api/v1/appointment/doctor/${doctorID}`, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json; charset=utf-8' },
                body: JSON.stringify(formData),
            });
            const response = await res.json();
            console.log(response);
            // console.log(`info : ${response}`);

            // const res2 = await fetch(`api/v1/appointment/${response.id}`);
            // const appointmentID = await res2.json();
            // console.log(appointmentID)
            // console.log(appointmentID.id)

            if (response.message) {
                alert(response.message)
                return;
            }

            if (!formData) {
                window.location = `/appointment.html?id=${doctorID}`;
            }

            if (res.status === 401) {
                alert("請先登入再預約");
                window.location = '/login.html';
                return;
            }

            else {
                console.log(response.data.appointmentInfo.id)
                window.location = `/appointment_confirmation.html?id=${response.data.appointmentInfo.id}`;
            }
        } catch (err) {
            console.error(err);
        }
    })
}



// Altera a data atráves do formulário
function setDate(form) {
    let newDate = new Date(form.date.value);
    date = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate() + 1);
    generateCalendar();
    return false;
}

// Método Muda o mês e o ano do topo do calendário
function changeHeader(dateHeader) {
    const month = document.getElementById("month-header");
    if (month.childNodes[0]) {
        month.removeChild(month.childNodes[0]);
    }
    const headerMonth = document.createElement("h1");
    const textMonth = document.createTextNode(months[dateHeader.getMonth()].substring(0, 3) + " " + dateHeader.getFullYear());
    headerMonth.appendChild(textMonth);
    month.appendChild(headerMonth);
}

// Função para mudar a cor do botão do dia que está ativo
function changeActive() {
    let btnList = document.querySelectorAll('button.active');
    btnList.forEach(btn => {
        btn.classList.remove('active');
    });
    btnList = document.getElementsByClassName('btnDay');

    for (let i = 0; i < btnList.length; i++) {
        const btn = btnList[i];
        if (btn.textContent === (date.getDate()).toString()) {
            btn.classList.add('active');
        }
    }
}

// Função que pega a data atual
// function resetDate() {
//     date = new Date();
//     generateCalendar();
// }

// Muda a data pelo numero do botão clicado
function changeDate(button) {
    let newDay = parseInt(button.textContent);
    date = new Date(date.getFullYear(), date.getMonth(), newDay);
    generateCalendar();
}

// Funções de avançar e retroceder mês e dia
function nextMonth() {
    date = new Date(date.getFullYear(), date.getMonth() + 1, 1);
    generateCalendar(date);
}

function prevMonth() {
    date = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    generateCalendar(date);
}


function prevDay() {
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1);
    generateCalendar();
}

function nextDay() {
    date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);
    generateCalendar();
}

// document.onload = generateCalendar(date);

// // Submit form - still writing
// function submitDateAndTime() {
//     const subForm = document.getElementById('date-search');
//     console.log(subForm)
//     subForm.addEventListener('click', async (e) => {
//         try {
//             e.preventDefault();
//             console.log('loading')
//             const urlParams = new URLSearchParams(window.location.search);
//             const doctorID = urlParams.get("id")
//             console.log(doctorID)

//             // const res = await fetch(`/appointment?doctorID=${doctorID}`);
//             // const docData = res.json();

//             const date = subForm.btnDay.value;
//             const time = subForm.timeSlot.value;
//             console.log(`[info] ${subForm.btnDay.value}`)
//             // const appointmentNum = `${date} + ${doctorID} + ${1}`;

//             const formData = { date, time }
//             console.log(`[info] ${formData}`)

//             const res = await fetch(`api/v1/appointment/doctor/${doctorID}`, {
//                 method: 'POST',
//                 headers: { 'Content-Type': 'application/json; charset=utf-8' },
//                 body: JSON.stringify(formData),
//             });
//             const response = await res.json();
//             console.log(response);

//             if (res.status === 304) {
//                 window.location = '/appointment_confirmation.html';
//                 // alert(res.message);
//                 // console.log('error')
//             } else {
//                 console.log('hihihi')
//                 window.location = '/appointment_confirmation.html';
//             }
//         } catch (err) {
//             console.error(err);
//         }
//     })
// }

// Fetch user info
async function loadUserInfo() {
    console.log('userinfo')
    const res = await fetch('api/v1/appointment');
    const result = await res.json();
    document.querySelector(".name").innerHTML = userName;
    document.querySelector(".phone").innerHTML = userContact;
    userName = result.name;
    userContact = result.phone;
}

// Fetch doctor info
async function loadDoctorInfo() {
    console.log('doctorinfo')
    const urlParams = new URLSearchParams(window.location.search);
    const doctorID = urlParams.get("id")
    console.log(doctorID)
    const doctorRes = await fetch(`api/v1/appointment/doctor/${doctorID}`);
    const doctorData = await doctorRes.json();

    document.querySelector(".doctor-name").innerHTML = doctorData.doctorInfo[0].name;
    document.querySelector(".doctor-speciality").innerHTML = doctorData.doctorInfo[0].specialty;
    console.log(doctorData)
}

// Generate time-slot - 搬咗入 generate calendar 度寫
async function loadTimeSlot() {
    // Generate time-slot
    const urlParams = new URLSearchParams(window.location.search);
    const doctorID = urlParams.get("id")
    const doctorTimeRes = await fetch(`api/v1/appointment/doctorTime/${doctorID}`);
    const timeSlots = await doctorTimeRes.json();
    console.log(timeSlots)

    var optionDiv = document.getElementById('time-slot');

    let timeSlotss = timeSlots.timeSlot[0]
    console.log(timeSlots.timeSlot[0])
    console.log(timeSlots.timeSlot[0][0].start)

    for (let tS of timeSlotss) {

        console.log(tS.start)
        console.log(tS.end)

        let timeStart = tS.start
        let timeEnd = tS.end
        timeSlot = [timeStart, timeEnd]
        for (var timeSection of timeSlot) {

            var pickedDay = document.querySelector("td").addEventListener('click', () => {
                console.log(pickedDay)

                // get Selected TimeSlot
                console.log('fuck')
                var options = pickedDay
                console.log(options)
                // selectTime = parseInt(options.nodeValue)
                // console.log(selectTime)
            })

            if (tS.day === 1) {
                var x = document.createElement("option");
                x.setAttribute("id", "timeSlot");
                x.setAttribute("class", "timeSlot");
                var t = document.createTextNode(timeSection);
                x.appendChild(t);
                document.getElementById("time-select").appendChild(x);
                // console.log(t)
                x.setAttribute("value", timeSlot);
            }

        }
    }
}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}

// // Selected time-slot
// var selectTime = document.getElementById("time-select");
// selectTime.addEventListener('change', function () {
//     // get Selected TimeSlot
//     console.log('selected time-slot:')
//     var options = selectTime.value
//     console.log(options)
//     // selectTime = parseInt(options.nodeValue)
//     // console.log(selectTime)
// });


