window.onload = function () {
    loginCheck();
    checkQueueActive();
    loadAndDisplayClinicInfo();
    loadAndDisplayCurrentServingTicket();
    loadAndDisplayReadyTicket();
    loadAndDisplayYourTicketNumber();

    const socket = io.connect();

    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    // update now serving ticket number
    socket.on(`${clinicID}_updateServingTicket`, (update) => {

        if (update.statusID == 2) {
            document.querySelector('.now_serving').innerHTML = `目前應診籌號：${update.ticketNumber}`;
        }

    });

    // update next ticket number
    socket.on(`${clinicID}_updateNextTicket`, (update) => {
        document.querySelector('.current_number').innerHTML = update.nextTicket;
    });

    // update ready ticket number
    socket.on(`${clinicID}_updateReadyTickets`, (update) => {

        const tickets = update.readyTickets.map(function (tickets) {
            return tickets.ticket_number;
        }).join("，")

        document.querySelector('.standby').innerHTML = `以下籌號請準備見醫生：${tickets}`;
    });

    // clinic set ticket number
    socket.on(`${clinicID}_clinicUpdateNextTicket`, (update) => {

        document.querySelector('.current_number').innerHTML = update.nextTicket;
    });


};

async function loadAndDisplayClinicInfo() {

    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    const res = await fetch(`/api/v1/getClinicInfo/${clinicID}`);

    if (res.status === 400) {
        alert("Clinic Does Not Exist");
        window.location = '/searchClinic.html';
        return;
    }

    const data = await res.json();

    // console.log(data)
    // console.log(data.clinicInfo)
    // console.log(data.doctorNameList)

    let doctorNames = data.doctorNameList.map(function (doctorNameList) {
        return doctorNameList['name'];
    });

    doctorNames = doctorNames.join(' ');

    let info = `
    <div class="clinic_info">
                        <div class="icon"><i class="fas fa-clinic-medical"></i></div>
                        <div class="clinic_name">${data.clinicInfo.name}</div>
                        <div class="doctor_name">${doctorNames}</div>
                        <div class="address">
                            <div class="info_title">地址：</div>
                            <div class="info_content">${data.clinicInfo.address}</div>
                        </div>

                        <div class="contact">
                            <div class="info_title">聯絡電話：</div>
                            <div class="info_content">${data.clinicInfo.phone}</div>
                        </div>

                        <div class="opening_hours">
                            <div class="info_title">開放時間：</div>
                            <div class="info_content">
                               ${data.clinicInfo.opening_hours}
                            </div>
                        </div> 
    
    </div>
    `
    document.querySelector('.clinic_info_container').innerHTML = info;
}

async function loadAndDisplayCurrentServingTicket() {

    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    // 1. Check if any ticket seeing doctor 
    res = await fetch(`/api/v1/getCurrentServingTickets/${clinicID}`);
    servingNumbers = await res.json();

    if (servingNumbers.length > 0) {

        let maxTicketNum = findMaxNumber(servingNumbers);

        let currentServingString = `目前應診籌號：${maxTicketNum}`
        document.querySelector('.now_serving').innerHTML = currentServingString;

        return;
    }

    // 2. Check if any ticket last served 
    res = await fetch(`/api/v1/getLastServedTickets/${clinicID}`);
    servingNumbers = await res.json();

    if (servingNumbers.length > 0) {

        let maxTicketNum = findMaxNumber(servingNumbers);

        let currentServingString = `目前應診籌號：${maxTicketNum}`
        document.querySelector('.now_serving').innerHTML = currentServingString;

        return;
    }

}

async function loadAndDisplayReadyTicket() {
    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    const res = await fetch(`/api/v1/getReadyTickets/${clinicID}`);
    const readyTickets = await res.json();

    const tickets = readyTickets.map(function (tickets) {
        return tickets.ticket_number;
    }).join("，")

    document.querySelector('.standby').innerHTML = `以下籌號請準備見醫生：${tickets}`;

}

function findMaxNumber(servingNumbers) {
    const maxNumberTicket = servingNumbers.reduce(function (previous, current) {
        if (current.ticket_number > previous.ticket_number) {
            return current
        } else {
            return previous
        }
    }, { 'ticket_number': 1 }).ticket_number

    return maxNumberTicket;
}

async function loadAndDisplayYourTicketNumber() {
    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    const res = await fetch(`/api/v1/getYourTicketNumber/${clinicID}`);
    let yourTicketNumber = await res.json();

    if (yourTicketNumber === null) {
        yourTicketNumber = " ";

        getTicketButton.innerHTML = "暫停取籌";
        getTicketButton.style.pointerEvents = "none"
    }

    let yourTicketString =
        `<div>下個取票籌號</div>
     <div class="current_number">${yourTicketNumber}</div>`

    document.querySelector('.ticket').innerHTML = yourTicketString;

}

async function getNewTicket() {

    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    const res = await fetch(`/api/v1/getNewTicket/${clinicID}`);

    if (res.status === 401) {
        alert("請先登入再取籌");
        window.location = '/login.html';
        return;
    }

    if (res.status === 200) {
        const ticketID = await res.json()

        let ticketIDString = ticketID[0].toString();
        let encodedticketID = btoa(ticketIDString)

        window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
        return;
    }
}

const getTicketButton = document.querySelector('.get_ticket')

async function checkQueueActive() {

    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID");

    const res = await fetch(`/api/v1/checkQueueActive/${clinicID}`);
    const result = await res.json();
    const status = result[0].queue_active;

    if (status == true) {

        const res = await fetch(`/api/v1/getYourTicketNumber/${clinicID}`);
        let yourTicketNumber = await res.json();

        if (yourTicketNumber === null) {
            yourTicketNumber = " ";
            getTicketButton.innerHTML = "暫停取籌";
            getTicketButton.style.pointerEvents = "none"
            return;
        }

        getTicketButton.innerHTML = "取籌";

        return;

    } else {
        getTicketButton.innerHTML = "暫停取籌";
        getTicketButton.style.pointerEvents = "none"
        return;
    }

}

async function checkGetTicketNot() {
    const res = await fetch(`/api/v1/checkGetTicketNot`);

    if (res.status === 400) {
        alert("請先登入再取籌");
        window.location = '/login.html';
        return;
    }

    const result = await res.json();

    if (result.length > 0) {
        alert("你已取籌");
        window.location = '/index.html'
        return;
    } else {
        getNewTicket()
    }
}

getTicketButton.addEventListener('click', () => {
    checkGetTicketNot();
})

let loggedInbuttonSet = `
        <div class="navbar" role="group" aria-label="navbar">
        <button onclick="window.location.href='/bookmark.html'" type="button" class="btn btn-primary">
        <i class="fas fa-bookmark"></i>
        </button>
        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
        class="fas fa-home"></i>
        <button onclick="getOwnTicket()" type="button" class="btn btn-primary own-ticket">
            <i class="fas fa-ticket-alt"></i>
        </button>
        <button onclick="window.location.href='/patientProfile.html'" type="button" class="btn btn-primary">
            <i class="fas fa-address-card"></i>
        </button>
        </div>
        `
let visitorButtonSet = `
        <div class="navbar" role="group" aria-label="navbar">

        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
                class="fas fa-home"></i>

        </button>
        <button type="submit" id="login-button" onclick="window.location.href='/login.html'">登入</button>
        </div>
        `

async function loginCheck() {
    const res = await fetch('/api/v1/loginCheck');

    const data = await res.json();

    if (data.message === "LoggedIn") {
        document.querySelector(".reserved").innerHTML = loggedInbuttonSet
        // document.querySelector("#login-button").style.display = "none";
    } else {
        document.querySelector(".reserved").innerHTML = visitorButtonSet
        // document.querySelector("#login-button").style.display = "block";
    }
}