function numberToWorkday( day ) {

    const numberToWorkdayObject = {
        1: "星期一",
        2: "星期二",
        3: "星期三",
        4: "星期四",
        5: "星期五",
        6: "星期六",
        7: "星期日",
    }

    return numberToWorkdayObject[day];
}

function districtToChinese( district ) {
    const districtToChineseObject = {
        central_and_western: "中西區",
        eastern: "東區",
        southern: "南區",
        wai_chai: "灣仔",
        sham_shui_po: "深水埗",
        kowloon_city: "九龍城",
        kwun_tong: "觀塘",
        wong_tai_sin: "黃大仙",
        yau_tsim_mong: "油尖旺",
        islands: "離島",
        kwai_tsing: "葵青",
        north: "北區",
        sai_kung: "西貢",
        sha_tin: "沙田",
        tai_po: "大埔",
        tsuen_wan: "荃灣",
        tuen_mun: "屯門",
        yuen_long: "元朗",
    }

    return districtToChineseObject[district];

}

function specialtyToChinese( specialty ) {
    const specialtyToChineseObject = {
        general_practice: "全科",
        cardiology: "心臟科",
        dermatology: "皮膚科",
        endocrinology_diabetes_and_metabolism: "內分泌及糖尿科",
        family_medicine: "家庭醫學",
        gastroenterology_and_hepatology: "腸胃肝臟科",
        general_surgery: "外科",
        haematology_andhaematological_oncology: "血液及血液腫瘤科",
        internal_medicine: "內科",
        medical_oncology: "腫瘤科",
        nephrology: "腎病科",
        neurology: "腦神經科",
        neurosurgery: "神經外科",
        obstetrics_and_gynaecology: "婦產科",
        ophthalmology: "眼科",
        orthopaedic_and_traumatology: "骨科",
        otolaryngology: "耳鼻喉科",
        psychiatry: "精神科",
        radiology: "放射科",
        respiratory_medicine: "呼吸系統科",
        rheumatology: "風濕病科",
        urology: "泌尿外科",
        clinical_oncology: "臨床腫瘤學",
        paediatrics: "兒科"
    }

    return specialtyToChineseObject[specialty];

}

function spaceToPlus ( address ) {

    let addressArr = address.split(" ")

    let addressWithPlus = addressArr.join("+")

    return addressWithPlus;
}