let topDoctors = '<h2><i class="far fa-thumbs-up"></i> 推薦醫生</h2>';
let searchStatus = "doctor";
let loggedInbuttonSet = `
        <div class="navbar" role="group" aria-label="navbar">
        <button onclick="window.location.href='/bookmark.html'" type="button" class="btn btn-primary">
        <i class="fas fa-bookmark"></i>
        </button>
        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
        class="fas fa-home"></i>
        <button onclick="getOwnTicket()" type="button" class="btn btn-primary own-ticket">
            <i class="fas fa-ticket-alt"></i>
        </button>
        <button onclick="window.location.href='/patientProfile.html'" type="button" class="btn btn-primary">
            <i class="fas fa-address-card"></i>
        </button>
        </div>
        `
let visitorButtonSet = `
        <div class="navbar" role="group" aria-label="navbar">

        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
                class="fas fa-home"></i>

        </button>
        <button type="submit" id="login-button" onclick="window.location.href='/login.html'">登入</button>

        </div>
        `

function changeCriteria(criteria){
    if (criteria === "clinic") {
        searchStatus = "clinic"
        document.querySelector("#specialty").style.display = "none"
        document.querySelector(".order > select").style.display = "none"
        // document.querySelector(".order > label").style.display = "none"
        document.querySelector("#search-input").style.backgroundColor = "lightgray"

    } else if (criteria === "doctor") {
        searchStatus = "doctor"
        document.querySelector("#specialty").style.display = "block"
        document.querySelector(".order > select").style.display = "block"
        // document.querySelector(".order > label").style.display = "block"
        document.querySelector("#search-input").style.backgroundColor = "white"
    }
}



async function loginCheck() {
    const res = await fetch('/api/v1/loginCheck');

    const data = await res.json();

    if (data.message === "LoggedIn") {
        document.querySelector(".reserved").innerHTML = loggedInbuttonSet
        // document.querySelector("#login-button").style.display = "none";
    } else {
        document.querySelector(".reserved").innerHTML = visitorButtonSet
        // document.querySelector("#login-button").style.display = "block";
    }

}

async function getTopDoctors() {

    const res = await fetch(`/api/v1/getTopDoctors/`);
    const data = await res.json();

    for (const doctorProfile of data.topDoctorProfiles) {

        let roundedRating = Math.round(doctorProfile.rating)

        let stars = "";

        let x = 0;

        while (x < 5) {

            if (x < roundedRating) {
                stars += '<span class="fa fa-star checked"></span>'
            } else {
                stars += '<span class="fa fa-star"></span>'
            }
            x++;
        }

        let specialties = "";

        for (let specialty of doctorProfile.specialties) {
            specialties += `[${specialtyToChinese(specialty.specialty)}] `
        }

        topDoctors += `
            <div class="doctors">
                <img src="./img/doctor_dummy_img.png" alt="doctor lee" height="100" width="100">
                <div><span class="doctor-name">${doctorProfile.name}</span><br>
                    <span class="specialty">${specialties}</span><br>
                    評分： <span class="score">
                        ${stars}
                    </span>
                    <div class="doctor-buttons">
                        <button onclick="window.location.href='/appointment.html?id=${doctorProfile.id}'">
                            預約
                        </button>
                        <button onclick="window.location.href='/doctorProfile.html?doctorID=${doctorProfile.id}'">
                            詳請
                        </button>
                    </div>
                </div>
            </div>
        `;

    }

    document.querySelector(".doctors-wall").innerHTML = topDoctors;

}

async function initSearch() {

    const form = document.querySelector("#search-form");
    form.addEventListener("submit", async function (event) {

        event.preventDefault();

        const formObject = {
            orderby: form.order.value,
            district: form.district.value,
            specialty: form.specialty.value,
            keywords: form.keywords.value,
        };

        if (searchStatus === "doctor") {
            const res = await fetch('/api/v1/searchDoctor', {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formObject),
            })

            const data = await res.json()

            let resultDoctors = "";
            for (const doctorProfile of data.resultDoctors) {

                let roundedRating = Math.round(doctorProfile.rating)

                let stars = "";

                let x = 0;

                while (x < 5) {

                    if (x < roundedRating) {
                        stars += '<span class="fa fa-star checked"></span>'
                    } else {
                        stars += '<span class="fa fa-star"></span>'
                    }
                    x++;
                }

                let specialties = "";

                for (let specialty of doctorProfile.specialties) {
                    specialties += `[${specialtyToChinese(specialty.specialty)}] `
                }

                resultDoctors += `
                    <div class="doctors">
                        <img src="./img/doctor_dummy_img.png" alt="doctor lee" height="100" width="100">
                        <div>名稱：<span class="doctor-name">${doctorProfile.name}</span><br>
                            <span class="specialty">${specialties}</span><br>
                            評分： <span class="score">
                                ${stars}
                            </span>
                            <div class="doctor-buttons">
                                <button onclick="window.location.href='/appointment.html?id=${doctorProfile.id}'">
                                    預約
                                </button>
                                <button onclick="window.location.href='/doctorProfile.html?doctorID=${doctorProfile.id}'">
                                    詳情
                                </button>
                            </div>
                        </div>
                    </div>
                `;
            }

            if (res.status !== 200) {
                alert(data.message);
                return
            }

            document.querySelector(".doctors-wall").innerHTML = resultDoctors;
        } else if (searchStatus === "clinic") {
            const res = await fetch('/api/v1/searchClinic', {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(formObject),
            })

            const data = await res.json()

            let resultClinics = "";

            for (let resultClinic of data.resultClinic) {
                console.log(resultClinic)

                let waitingNumber = 0;
                for (let ticket of resultClinic.tickets) {
                    if (ticket.ticket_number){
                        waitingNumber += 1;
                    }
                }

                let queue = ""

                if (resultClinic.queue_active) {
                    queue += `<span class="queue-active"><span class="dot green"></span> 現在開放排籌</span>`
                } else {
                    queue += `<span class="queue-active"><span class="dot red"></span> 排籌暫停`
                }

                resultClinics += `
                <div class="result-clinic">
                    <div>名稱：<span class="clinic-name">${resultClinic.name}</span><br>
                        地區： <span class="district">${districtToChinese(resultClinic.district)}</span><br>
                        現時輪候人數： <span class="waiting-numbers">${waitingNumber}</span><br>
                        ${queue}
                        <div class="result-clinic-buttons">
                            <button onclick="window.location.href='/getTicket.html?clinicID=${resultClinic.id}'">
                                排籌
                            </button>
                            <button onclick="window.location.href='/clinicProfile.html?clinicID=${resultClinic.id}'">
                                詳情
                            </button>
                        </div>
                    </div>
                </div>
                `
            }

            document.querySelector(".doctors-wall").innerHTML = resultClinics;

        }

    })

}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}


window.onload = () => {
    loginCheck();
    getTopDoctors();
    initSearch();
    
}

