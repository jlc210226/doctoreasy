let searchStatus = "doctor";
let loggedInbuttonSet = `
        <div class="navbar" role="group" aria-label="navbar">
        <button onclick="window.location.href='/bookmark.html'" type="button" class="btn btn-primary">
        <i class="fas fa-bookmark"></i>
        </button>
        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
        class="fas fa-home"></i>
        <button onclick="getOwnTicket()" type="button" class="btn btn-primary own-ticket">
            <i class="fas fa-ticket-alt"></i>
        </button>
        <button onclick="window.location.href='/patientProfile.html'" type="button" class="btn btn-primary">
            <i class="fas fa-address-card"></i>
        </button>
        </div>
        `
let visitorButtonSet = `
        <div class="navbar" role="group" aria-label="navbar">

        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
                class="fas fa-home"></i>
        </button>
        <button type="submit" id="login-button" onclick="window.location.href='/login.html'">登入</button>

        </div>
        `

async function loginCheck() {
    const res = await fetch('/api/v1/loginCheck');

    const data = await res.json();

    if (data.message === "LoggedIn") {
        document.querySelector(".reserved").innerHTML = loggedInbuttonSet
    } else {
        document.querySelector(".reserved").innerHTML = visitorButtonSet
    }
}
        

async function loadAndDisplayClinicInfo() {
    const urlParams = new URLSearchParams(window.location.search);
    const clinicID = urlParams.get("clinicID")

    const res = await fetch(`/api/v1/getClinicInfo/${clinicID}`);

    if (res.status === 400) {
        alert("Clinic Does Not Exist");
        window.location = '/searchClinic.html';
        return;
    }

    const data = await res.json();

    console.log(data)

    // console.log(data)
    // console.log(data.clinicInfo)
    // console.log(data.doctorNameList)

    let doctorNames = data.doctorNameList.map(function (doctorNameList) {
        return doctorNameList['name'];
    });

    doctorNames = doctorNames.join(' ');


    let info = `
    <div class="clinic_info">
                        <div class="icon"><i class="fas fa-clinic-medical"></i></div>
                        <div class="clinic_name">${data.clinicInfo.name}</div>
                        <div class="doctor_name">${doctorNames}</div>
                        <div class="address">
                            <div class="info_title">地址：</div>
                            <div class="info_content">${data.clinicInfo.address}</div>
                        </div>

                        <div class="contact">
                            <div class="info_title">聯絡電話：</div>
                            <div class="info_content">${data.clinicInfo.phone}</div>
                        </div>

                        <div class="opening_hours">
                            <div class="info_title">開放時間：</div>
                            <div class="info_content">
                               ${data.clinicInfo.opening_hours}
                            </div>
                        </div> 
                        <div>
                        <iframe width="250" height="250" style="border:0" loading="lazy" allowfullscreen
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDGXaqkNfuhBOIHQ2MbSoljYRmVT9_HYRI&q=${data.clinicInfo.name}"></iframe>
                        </div>
                        <button onclick="window.location.href='/getTicket.html?clinicID=${data.clinicInfo.id}'" class="get_ticket">詳細輪候情況</button>
    </div>




    `
    document.querySelector('.clinic_info_container').innerHTML = info;
}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}

window.onload = function () {
    loginCheck()
    loadAndDisplayClinicInfo()
};
