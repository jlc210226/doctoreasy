window.onload = () => {
    login()
}

function login() {
    const loginForm = document.getElementById("login-form")
    loginForm.addEventListener("submit", async function (event) {
        event.preventDefault();
        const formObject = {};
        formObject["username"] = loginForm.username.value;
        formObject["password"] = loginForm.password.value;

        const res = await fetch("/api/v1/clinicLogin", {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })

        const result = await res.json();
        if (res.status === 200) {
            console.log(result.message)
            window.location = "/clinic/clinicHomePage.html"
        }else{
            alert(result.message)
        }
    })
}