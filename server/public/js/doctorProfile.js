let loggedInbuttonSet = `
        <div class="navbar" role="group" aria-label="navbar">
        <button id="bookmark-page" onclick="window.location.href='/bookmark.html'" type="button" class="btn btn-primary">
        <i class="fas fa-bookmark"></i>
        </button>
        <button onclick="bookmarkDoctor()" id="bookmark" type="button" class="btn btn-primary"><i
                class="fas fa-star"></i>
        </button>
        <button onclick="window.location.href='/'" type="button" class="btn btn-primary">
            <i class="fas fa-home"></i>

        </button>
        <button onclick="getOwnTicket()" type="button" class="btn btn-primary own-ticket">
            <i class="fas fa-ticket-alt"></i>
        </button>
        <button onclick="window.location.href='/patientProfile.html'" type="button" class="btn btn-primary"><i
                class="fas fa-address-card"></i>

        </button>
        </div>
        `
let visitorButtonSet = `
        <div class="navbar" role="group" aria-label="navbar">

        <button onclick="window.location.href='/'" type="button" class="btn btn-primary"><i
                class="fas fa-home"></i>

        </button>
        <button type="submit" id="login-button" onclick="window.location.href='/login.html'">登入</button>
        </div>
        `

const urlParams = new URLSearchParams(window.location.search);
const doctorId = urlParams.get("doctorID")
let IsLogin = false;

async function loginCheck() {
    const res = await fetch('/api/v1/loginCheck');

    const data = await res.json();

    if (data.message === "LoggedIn") {
        document.querySelector(".reserved").innerHTML = loggedInbuttonSet;
        IsLogin = true; 
        
    } else {
        document.querySelector(".reserved").innerHTML = visitorButtonSet
        document.querySelector("#comment-id").style.display = "none"
    }

}

async function getDoctor() {

    const res = await fetch(`/api/v1/getDoctorInfo/${doctorId}`);
    const res1 = await fetch(`/api/v1/getDoctorSpecialties/${doctorId}`)

    const data = await res.json();
    const specialties = await res1.json();

    console.log(data);
    console.log(specialties)

    let specialtiesStr =``;

    for (let key in specialties){
        specialtiesStr += `${specialtyToChinese(specialties[key].specialty)} `
    }

    console.log(specialtiesStr)

    let roundedRating = Math.round(data.doctorProfile.rating)

    let stars = "";

    let x = 0;

    while (x < 5) {

        if (x < roundedRating) {
            stars += '<span class="fa fa-star checked"></span>'
        } else {
            stars += '<span class="fa fa-star"></span>'
        }
        x++;
    }

    let openingHours = "";

    let officeHour = data.doctorProfile.office_hour;

    let openingHoursArr = data.doctorProfile.opening_hours.split(", ");

    let address = spaceToPlus(data.doctorProfile.clinic_address)

    console.log(address)

    for (item of openingHoursArr) {
        openingHours += `${item}<br>`
    }

    doctorString = `
    <img src="./img/doctor_dummy_img.png" alt="doctor_dummy_img" width="200">

    <div>名稱：<span class="doctor-name">${data.doctorProfile.name}</span><br>
        專業： <span class="specialty">${specialtiesStr}</span><br>
        性別： <span class="gender">${data.doctorProfile.gender}</span><br>
        評分： <span class="score">
        ${stars}
        </span><br>

        <div class="info">
            <h6>工作時間</h6>
            <span>

        <table style="width:100%">
                        <tr>
                            <th></th>
                            <th>AM</th>
                            <th>PM</th>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[0].day)}</td>
                            <td>${officeHour[0].start} - ${officeHour[0].end}</td>
                            <td>${officeHour[1].start} - ${officeHour[1].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[2].day)}</td>
                            <td>${officeHour[2].start} - ${officeHour[2].end}</td>
                            <td>${officeHour[3].start} - ${officeHour[3].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[4].day)}</td>
                            <td>${officeHour[4].start} - ${officeHour[4].end}</td>
                            <td>${officeHour[5].start} - ${officeHour[5].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[6].day)}</td>
                            <td>${officeHour[6].start} - ${officeHour[6].end}</td>
                            <td>${officeHour[7].start} - ${officeHour[7].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[8].day)}</td>
                            <td>${officeHour[8].start} - ${officeHour[8].end}</td>
                            <td>${officeHour[9].start} - ${officeHour[9].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[10].day)}</td>
                            <td>${officeHour[10].start} - ${officeHour[10].end}</td>
                            <td>${officeHour[11].start} - ${officeHour[11].end}</td>
                        </tr>
                        <tr>
                            <td>${numberToWorkday(officeHour[12].day)}</td>
                            <td>${officeHour[12].start} - ${officeHour[12].end}</td>
                            <td>${officeHour[13].start} - ${officeHour[13].end}</td>
                        </tr>
                    </table>
            </span><br>
        </div>
        <div class="result-clinic-buttons">
            <button onclick="window.location.href='/appointment.html?id=${data.doctorProfile.id}'">
                預約
            </button>
        </div>
    </div>
    `
    clinicString = `
    <div class="clinic-info">
        <div>
            <div class="info">
                名稱：<span class="clinic-name">${data.doctorProfile.clinic_name}</span><br>
                
                開診時間： <br>
                <span class="working-hours">
                ${openingHours}
                </span><br>
            </div>

            <div class="result-clinic-buttons">
                <button onclick="window.location.href='/getTicket.html?clinicID=${data.doctorProfile.clinic_id}'">
                    排籌
                </button>
                <button onclick="window.location.href='/clinicProfile.html?clinicID=${data.doctorProfile.clinic_id}'">
                    詳情
                </button>
            </div>
        </div>
    </div>
    <iframe width="250" height="250" style="border:0" loading="lazy" allowfullscreen
        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDGXaqkNfuhBOIHQ2MbSoljYRmVT9_HYRI&q=${data.doctorProfile.clinic_name}"></iframe>
    </div>
    `

    document.querySelector(".doctor").innerHTML = doctorString;
    document.querySelector(".clinic").innerHTML = clinicString;

    if (res.status !== 200) {
        alert(data.message);
    }

}

async function getComment() {

    console.log(doctorId)
    const res = await fetch(`/api/v1/getComment/${doctorId}`)
    const data = await res.json();
    console.log(data.commentInfo)
    if (data.commentInfo) {
        let commentStr = ``
        for (let info of data.commentInfo) {
            // commentStr += `<div class="comment">用戶：<span class="comment-name">${info.username}</span><p>留言：${info.comment}</p></div>`
            commentStr += `
            <div class="comment">
            <div class="comment-user">
                <div>用戶：</div>
                <div class="comment-name">${info.username}</div>
            </div>
        

        <div class="comment-content">
            <div>留言：</div>
            <p>${info.comment}</p>
        </div>
    </div>
            `
        }
        console.log(document.querySelector(".comment-box"))
        document.querySelector(".comment-box").innerHTML = commentStr;
    } else {
        console.log(data.message)
    }
}

function createComment() {
    const commentForm = document.getElementById("comment-id")

    commentForm.addEventListener("submit", async function (event) {
        event.preventDefault();
        console.log(event)
        const formObject = {};
        formObject["comment"] = commentForm.comment.value;
        formObject["grading"] = commentForm.grading.value;
        console.log(formObject.grading)
        console.log(formObject.comment)
        const res = await fetch(`/api/v1/createComment/${doctorId}`, {
            method: "POST",
            headers: {
                "Content-type": "application/json; charset=utf-8",
            },
            body: JSON.stringify(formObject)
        })
        const data = await res.json()
        if (res.status === 200) {
            await getComment()
            await getDoctor()
            commentForm.comment.value = ""
            commentForm.grading.value = ""
        } else {
            console.log(data.message)
        }
    })
}

async function bookmarkDoctor() {

    console.log(doctorId)
    const res = await fetch(`/api/v1/bookmark/${doctorId}`)
    const data = await res.json()
    if (res.status === 200) {
        console.log(data.message)
        console.log(document.querySelector(".fa-star"))
        document.getElementById("bookmark").style.display = "none"
    } else {
        console.log(data.message)
    }
}

async function checkBookmark() {
    if (IsLogin) {
        const res = await fetch(`/api/v1/checkBookmark/${doctorId}`)
        const data = await res.json()
        console.log(data)
        if (res.status === 200 && data.message === "already bookmarked") {
            document.getElementById("bookmark").style.display = "none"
    
        } else {
            console.log(data.message)
        }
    }
}

async function getOwnTicket() {

    const res = await fetch("/api/v1/getOwnTicket")
    const data = await res.json();

    if (data == '') {
        alert("你尚未取籌。");
        return;
    }

    const ticketID = data[0].id;
    console.log(ticketID)

    let ticketIDString = ticketID.toString();
    let encodedticketID = btoa(ticketIDString)

    window.location = `/ticketConfirm.html?ticketID=${encodedticketID}`;
    return;

}

window.onload = () => {
    loginCheck();
    getDoctor();
    getComment()
    checkBookmark()
    createComment()
}