import express from "express";
import { userController } from "../main";

export const userRoutes = express.Router();

userRoutes.post("/login", userController.login);
userRoutes.post("/clinicLogin", userController.cliniclogin);
userRoutes.get("/login/google", userController.loginGoogle);
userRoutes.post("/register&createProfile", userController.registerAndCreateProfile);
userRoutes.get("/loginCheck", userController.checkLogin);
userRoutes.get("/logout", userController.logout);
userRoutes.get("/getUserInfo", userController.getUserInfo);