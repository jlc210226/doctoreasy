import express from "express";
import { clinicAppointmentController } from "../main"

export const clinicAppointmentRoutes = express.Router();

clinicAppointmentRoutes.put('/updateAppointmentStatus/:appointmentID/:statusID', clinicAppointmentController.updateAppointmentStatus)