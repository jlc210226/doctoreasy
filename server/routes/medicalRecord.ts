import express from "express";
import { medicalRecordController } from "../main";
export const medicalRecordRoutes = express.Router();


medicalRecordRoutes.get("/getAllMedicalRecord", medicalRecordController.getAllMedicalRecords);
medicalRecordRoutes.get("/getMedicalReport/:medicalReportId", medicalRecordController.getOwnMedicalRecord);
// medicalRecordRoutes.get("/getAllPatientMedicalRecord/:patientID", medicalRecordController.clinicGetAllMedicalRecords);
// medicalRecordRoutes.post("/createReport/:patientID", medicalRecordController.createMedicalRecord)