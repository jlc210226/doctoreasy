import express from "express";
import { ticketController } from "../main"

export const ticketRoutes = express.Router();

ticketRoutes.get('/getCurrentServingTickets/:clinicID', ticketController.getCurrentServingTickets)
ticketRoutes.get('/getLastServedTickets/:clinicID', ticketController.getLastServedTickets)
ticketRoutes.get('/getNewTicket/:clinicID', ticketController.getNewTicket)
ticketRoutes.get('/getYourTicketNumber/:clinicID', ticketController.getYourTicketNumber)
ticketRoutes.get('/getReadyTickets/:clinicID', ticketController.getReadyTickets)
ticketRoutes.get('/getTicketInfo/:ticketID', ticketController.getTicketInfo)
ticketRoutes.get('/checkGetTicketNot', ticketController.checkGetTicketNot)
// ticketRoutes.get('/clinicGetTicket', ticketController.clinicGetTicket)
// ticketRoutes.get('/startTicketQueue', ticketController.startTicketQueue)
// ticketRoutes.get('/stopTicketQueue', ticketController.stopTicketQueue)
// ticketRoutes.get('/clinicGetTodayTickets', ticketController.clinicGetTodayTickets)
// ticketRoutes.post('/clinicSetTicketNumber', ticketController.clinicSetTicketNumber)
// ticketRoutes.get('/clinicGetNextTicketNumber', ticketController.clinicGetNextTicketNumber)
ticketRoutes.get('/getOwnTicket', ticketController.getOwnTicket)
