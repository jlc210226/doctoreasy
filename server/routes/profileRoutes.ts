
import express from "express";
import { profileController } from '../main'


export const profileRoutes = express.Router();

profileRoutes.get('/getClinicInfo/:clinicID', profileController.getClinicInfo);
profileRoutes.get('/patientProfile/', profileController.getPatientProfile)
profileRoutes.get('/getDoctorInfo/:doctorID', profileController.getDoctorInfo);
profileRoutes.get('/getDoctorSpecialties/:doctorID', profileController.getDoctorSpecialties);
profileRoutes.get('/getComment/:doctorID', profileController.getComment);
profileRoutes.post('/createComment/:doctorID', profileController.createComment);
profileRoutes.get('/checkQueueActive/:clinicID', profileController.checkQueueActive)
profileRoutes.get('/bookmark/:doctorID', profileController.bookmarkDoctor);
profileRoutes.get('/getBookmarkDoctor/', profileController.getBookmarkDoctor);
profileRoutes.get('/checkBookmark/:doctorID', profileController.checkBookmarkDoctor);
profileRoutes.get('/cancelBookmark/:doctorID', profileController.cancelBookmark);
profileRoutes.get('/clinicpatientProfile/:patientID', profileController.clinicGetPatientProfile)
profileRoutes.get('/doctorGetPatientProfile/:patientID', profileController.doctorGetPatientInfo)
profileRoutes.get('/checkQueueActiveForClinic', profileController.checkQueueActiveForClinic);

