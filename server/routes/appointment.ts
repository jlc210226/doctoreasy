import express from "express";
import { appointmentController } from "../main";

export const appointmentRoutes = express.Router();

appointmentRoutes.get("/appointment/doctor/:id", appointmentController.loadDoctorInfo);
appointmentRoutes.get("/appointment/doctorTime/:id", appointmentController.loadTimeSlot);
appointmentRoutes.get("/appointment/:id", appointmentController.loadAppointmentInfo);
appointmentRoutes.post("/appointment/doctor/:id", appointmentController.createAppointment);
appointmentRoutes.post("/appointment/:id", appointmentController.createAppointment);
appointmentRoutes.get("/getAllAppoinment", appointmentController.loadAllAppointment);
appointmentRoutes.get("/getCurrentServingAppointments/:clinicID", appointmentController.getCurrentServingAppointments)
appointmentRoutes.get("/getLastServedAppointments/:clinicID", appointmentController.getlastServedAppointments)
appointmentRoutes.get("/appointment/doctorApp/:id", appointmentController.loadAllDoctorAppointment);
appointmentRoutes.get("/appointmentClinic", appointmentController.loadAppointmentInfoforClinic);