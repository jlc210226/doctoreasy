import express from "express";
import { clinicMedicalRecordController } from "../main"

export const clinicMedicalRecordRoutes = express.Router();

clinicMedicalRecordRoutes.get("/getAllPatientMedicalRecord/:patientID", clinicMedicalRecordController.clinicGetAllMedicalRecords);
clinicMedicalRecordRoutes.post("/createReport/:patientID", clinicMedicalRecordController.createMedicalRecord)