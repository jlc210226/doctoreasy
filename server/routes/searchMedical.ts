import express from "express";
import { searchMedicalController } from "../main";

export const searchMedicalRoutes = express.Router();

searchMedicalRoutes.get("/getTopDoctors", searchMedicalController.getTopDoctors);
searchMedicalRoutes.post("/searchDoctor", searchMedicalController.searchDoctor);
searchMedicalRoutes.post("/searchClinic", searchMedicalController.searchClinic);

searchMedicalRoutes.post("/searchPatient", searchMedicalController.searchPatient);