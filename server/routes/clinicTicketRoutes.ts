import express from "express";
import { clinicTicketController } from "../main"

export const clinicTicketRoutes = express.Router();

clinicTicketRoutes.get('/clinicGetTicket', clinicTicketController.clinicGetTicket)
clinicTicketRoutes.put('/updateTicketStatus/:ticketID/:statusID', clinicTicketController.updateTicketStatus)
clinicTicketRoutes.get('/startTicketQueue', clinicTicketController.startTicketQueue)
clinicTicketRoutes.get('/stopTicketQueue', clinicTicketController.stopTicketQueue)
clinicTicketRoutes.post('/clinicSetTicketNumber', clinicTicketController.clinicSetTicketNumber)
clinicTicketRoutes.get('/clinicGetNextTicketNumber', clinicTicketController.clinicGetNextTicketNumber)
clinicTicketRoutes.get('/clinicGetTodayTickets', clinicTicketController.clinicGetTodayTickets)
clinicTicketRoutes.get('/logout', clinicTicketController.clinicLogout)
clinicTicketRoutes.get('/clinicGetCurrentServingTicket', clinicTicketController.clinicGetCurrentServingTicket)
clinicTicketRoutes.get('/clinicGetLastServedTicket', clinicTicketController.clinicGetLastServedTickets)