import Knex from 'knex'

import { medicalRecord } from "../utils/models"
import { doctorProfileTableName, diagnosisReportTableName, patientProfileTableName } from "../utils/tables"

export class MedicalService {
    constructor(private knex: Knex) { }

    async getAllMedicalRecord(userID: number) {
        const patientID = await this.knex(patientProfileTableName)
            .where('user_patient_id', userID)
            .select('id')
            .first();
        const medicalInfo = await this.knex(diagnosisReportTableName)
            .innerJoin(doctorProfileTableName, `${diagnosisReportTableName}.doctor_profile_id`, 'doctor_profile.id',)
            .select(`${diagnosisReportTableName}.id`,`${doctorProfileTableName}.name`, `${diagnosisReportTableName}.diagnosis`, `${diagnosisReportTableName}.report_date`)
            .where('diagnosis_report.patient_id', patientID.id)
        console.log(`medicalInfo:${medicalInfo}`)
        return medicalInfo;
    }
    //Moved to clinicMedicalRecord
    // async clinicGetAllMedicalRecord(patientID: number) {
    //     const medicalInfo = await this.knex(diagnosisReportTableName)
    //         .innerJoin(doctorProfileTableName, `${diagnosisReportTableName}.doctor_profile_id`, 'doctor_profile.id',)
    //         .select(`${diagnosisReportTableName}.id`,`${doctorProfileTableName}.name`, `${diagnosisReportTableName}.diagnosis`, `${diagnosisReportTableName}.report_date`)
    //         .where('diagnosis_report.patient_id', patientID)
    //     console.log(`medicalInfo:${medicalInfo}`)
    //     return medicalInfo;
    // }

    
    // async getMedicalRecord(meicalID: number) {
    //     const patientID = await this.knex(patientProfileTableName)
    //         .where('user_patient_id', 1771)
    //         .select('id')
    //         .first();
    //     const medicalInfo = await this.knex<medicalRecord>(diagnosisReportTableName)
    //         .innerJoin(doctorProfileTableName, `${diagnosisReportTableName}.doctor_profile_id`, 'doctor_profile.id',)
    //         .where('patient_id', patientID.id)
    //         .select(`${doctorProfileTableName}.name`, `${diagnosisReportTableName}.id`, `${diagnosisReportTableName}.diagnosis`, `${diagnosisReportTableName}.report_date`)
    //     console.log(medicalInfo)
    //     return medicalInfo;
    // }
    async getOwnMedicalRecord(medicalReportID: number) {
        console.log(medicalReportID)
        const medicalInfo = await this.knex<medicalRecord>(diagnosisReportTableName)
            .innerJoin(doctorProfileTableName, `${diagnosisReportTableName}.doctor_profile_id`, 'doctor_profile.id',)
            .innerJoin(patientProfileTableName, `${diagnosisReportTableName}.patient_id`, `${patientProfileTableName}.id`)
            .select(`${patientProfileTableName}.name`, `${patientProfileTableName}.date_of_birth`,`${patientProfileTableName}.gender`, `${doctorProfileTableName}.name`, `${diagnosisReportTableName}.diagnosis`, `${diagnosisReportTableName}.report_date`, `${diagnosisReportTableName}.medication`, `${diagnosisReportTableName}.symptoms`, `${diagnosisReportTableName}.remark`)
            .column('diagnosis_report.*', 'doctor_profile.*' , { doctor_name: 'doctor_profile.name' }, { patient_name: 'patient_profile.name' }, 'patient_profile.*' )
            .where(`${diagnosisReportTableName}.id`, medicalReportID)
        console.log(medicalInfo)
        return medicalInfo;
    }

    async getClinicIDByDoctorID(doctorID:number) {
        const clinicID = await this.knex("user_medical")
        .select("clinic_profile_id")
        .where("id", doctorID)
        return clinicID;
    }

    async getDoctorProfileIDbyUserID(userID:number) {
        const docProfileID = await this.knex("doctor_profile")
        .select("id")
        .where("user_medical_id", userID)
        .first()
        return docProfileID
    }

    //moved to clinicmeicalrecord
    // async createMedicalRecord(patientID:number, doctorID:number, clinicID:number, report_date: Date, symptoms: string, diagnosis: string, medication: string, remark:string) {
    //     const id = await this.knex(diagnosisReportTableName)
    //     .insert({patient_id: patientID, clinic_id: clinicID, doctor_profile_id: doctorID, report_date: report_date, symptoms: symptoms, diagnosis: diagnosis, medication: medication, remark: remark})
    //     return id;
    // }
}


