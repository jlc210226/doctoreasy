import Knex from 'knex';
import { clinicProfileTableName, ticketTableName, doctorProfileTableName, userMedicalTableName, ticketStatusTableName, patientProfileTableName } from "../utils/tables";
import { redisClient } from "../main";
import util from 'util';

export class TicketService {
    constructor(private knex: Knex) {
    }

    async getCurrentServingTicket(clinicID: number) {

        const currentServingTickets = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 2
        }).where('ticket.created_at', ">", "'today'")


        return currentServingTickets;
    }

    async getLastServedTickets(clinicID: number) {

        const lastServedTickets = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 3
        }).where('ticket.created_at', ">", "'today'").select('ticket_number')

        return lastServedTickets;
    }

    async getReadyTickets(clinicID: number) {

        const readyTickets = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 1
        }).where('ticket.created_at', ">", "'today'")
            .select('ticket_number').limit(3).orderBy('id')

        return readyTickets;
    }

    async getNewTicket(clinicID: number, userID: number) {

        let patientID = await this.knex(patientProfileTableName).where({
            user_patient_id: userID
        }).select('id');

        patientID = patientID[0].id;

        const redisGet = util.promisify(redisClient.get).bind(redisClient)
        const redisSet = util.promisify(redisClient.set).bind(redisClient)

        const ticket = await redisGet(`${clinicID}`)

        const ticketID = await this.knex(ticketTableName).insert({
            'ticket_number': ticket,
            'patient_id': patientID,
            'clinic_id': clinicID,
            'status_id': 1
        }).returning('id')

        const nextTicket = parseInt(ticket) + 1;

        await redisSet(`${clinicID}`, `${nextTicket}`);

        return { ticketID, nextTicket };

    }

    async getYourTicketNumber(clinicID: number) {

        const redisGet = util.promisify(redisClient.get).bind(redisClient)

        const yourTicket = await redisGet(`${clinicID}`)

        return yourTicket;

    }

    async getTicketInfo(ticketID: string) {

        const info = await this.knex(ticketTableName)
            .innerJoin(clinicProfileTableName, 'ticket.clinic_id', 'clinic_profile.id')
            .select('ticket.ticket_number', 'ticket.clinic_id', 'ticket.created_at', 'clinic_profile.name', 'clinic_profile.address', 'clinic_profile.phone', 'clinic_profile.opening_hours')
            .where('ticket.id', ticketID)

        const clinicID = info[0].clinic_id;

        const doctorNames = await this.knex(doctorProfileTableName)
            .innerJoin(userMedicalTableName, 'doctor_profile.user_medical_id', 'user_medical.id')
            .innerJoin(clinicProfileTableName, 'clinic_profile.id', 'user_medical.clinic_profile_id')
            .select('doctor_profile.name')
            .where('clinic_profile.id', clinicID)

        const data = { info: info, doctorList: doctorNames }

        return data;

    }

    async checkGetTicketNot(userID: number) {

        let patientID = await this.knex(patientProfileTableName).where({
            user_patient_id: userID
        }).select('id');

        patientID = patientID[0].id;

        const result = await this.knex(ticketTableName).where({
            'patient_id': patientID,
            'status_id': 1,
        })
            .where('ticket.created_at', ">", "'today'")
            .select('id')
            .limit(1)
            .orderBy('id', "desc")

        return result;

    }

    async getTicketAllInfo(ticketID: string) {
        const allInfo = await this.knex(ticketTableName)
            .column("patient_profile.*", "ticket_status.status", "ticket.ticket_number", { ticket_id: 'ticket.id' }, { ticket_created_at: 'ticket.created_at' })
            // .select("patient_profile.*", "ticket_status.status", "ticket.ticket_number")
            .innerJoin(clinicProfileTableName, 'ticket.clinic_id', 'clinic_profile.id')
            .innerJoin(ticketStatusTableName, "ticket.status_id", "ticket_status.id")
            .innerJoin(patientProfileTableName, "ticket.patient_id", "patient_profile.id")
            .where('ticket.id', ticketID)

        return allInfo;

        //     return data;

    }

    async getOwnTicket(userID: number) {

        // Set UTC C

        let patientID = await this.knex(patientProfileTableName).where({
            user_patient_id: userID
        }).select('id');

        patientID = patientID[0].id;

        const ticket = await this.knex(ticketTableName).where({
            patient_id: patientID,
            status_id: 1,
        }).orderBy('ticket.created_at', 'desc')
            .where('ticket.created_at', ">", "'today'")
            .limit(1)

        return ticket;

    }
}

