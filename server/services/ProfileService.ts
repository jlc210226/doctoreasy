import Knex from 'knex'
import { Clinic, patientProfile, Doctor } from "../utils/models"
import { clinicProfileTableName, commentTableName, doctorOfficeHourTableName, doctorProfileTableName, doctorSpecialtyTableName, patientProfileTableName, specialtyTableName, userMedicalTableName } from "../utils/tables"

export class ProfileService {
    constructor(private knex: Knex) { }

    async getClinicInfo(clinicID: number) {

        const clinicInfo = await this.knex<Clinic>(clinicProfileTableName)
            .where('id', clinicID)
            .first();

        // console.log(clinicInfo)

        const doctorNames = await this.knex(doctorProfileTableName)
            .innerJoin(userMedicalTableName, 'doctor_profile.user_medical_id', 'user_medical.id')
            .innerJoin(clinicProfileTableName, 'clinic_profile.id', 'user_medical.clinic_profile_id')
            .select('doctor_profile.name')
            .where('clinic_profile.id', clinicID)

        const data = { clinicInfo: clinicInfo, doctorNameList: doctorNames }

        return data;
    }


    async getPatientProfile(patientID: number) {
        const patientProfile = await this.knex<patientProfile>(patientProfileTableName)
            .where('user_patient_id', patientID)
            .first();
        return patientProfile
    }
    async clinicGetPatientProfile(patientID: number) {
        const patientProfile = await this.knex<patientProfile>(patientProfileTableName)
            .where('id', patientID)
            .first();
        return patientProfile
    }
    async getDoctorInfo(doctorID: number) {
        const doctorInfo = await this.knex<Doctor>(doctorProfileTableName)
            .column('doctor_profile.*', 'clinic_profile.*', { doctor_name: 'doctor_profile.name' }, { clinic_id: 'clinic_profile.id' }, { clinic_name: 'clinic_profile.name' }, 'doctor_office_hour.*')
            .innerJoin(userMedicalTableName, 'doctor_profile.user_medical_id', 'user_medical.id')
            .innerJoin(clinicProfileTableName, 'clinic_profile.id', 'user_medical.clinic_profile_id')
            .innerJoin(doctorOfficeHourTableName, 'doctor_office_hour.doctor_profile_id', 'doctor_profile.id')
            .where('doctor_profile.id', doctorID)

        console.log(doctorInfo)

        let groupedDoctors = new Map();
        for (let item of doctorInfo) {
            const officeHour = {
                day: item.day,
                period: item.period,
                start: item.start,
                end: item.end
            };

            if (groupedDoctors.has(item.registration_no)) {
                groupedDoctors.get(item.registration_no).office_hour.push(officeHour)
            } else {
                const doctor: Doctor = {
                    id: item.id,
                    clinic_id: item.clinic_id,
                    name: item.doctor_name,
                    gender: item.gender,
                    registration_no: item.registration_no,
                    clinic_address: item.address,
                    profile_pic: item.profile_pic,
                    user_medical_id: item.user_medical_id,
                    rating: item.rating,
                    comment_count: item.comment_count,
                    office_hour: [officeHour],
                    opening_hours: item.opening_hours,
                    clinic_name: item.clinic_name,
                };
                groupedDoctors.set(item.registration_no, doctor)
            }
        }

        const doctorProfile = Array.from(groupedDoctors.values())[0]

        const data = { doctorProfile }

        return data;
    }

    async getDoctorSpecialties(doctorID: number) {
        const doctorSpecialties = await this.knex(doctorSpecialtyTableName)
            .select("specialty")
            .innerJoin(specialtyTableName, "doctor_specialty.specialty_id", "specialty.id")
            .where('doctor_specialty.doctor_profile_id', doctorID)

        return doctorSpecialties;
    }

    async getComment(doctorID: number) {
        const doctorInfo = await this.knex(commentTableName)
            .innerJoin("patient_profile", "comment.patient_id", "patient_profile.id")
            .innerJoin("user_patient", "patient_profile.user_patient_id", "user_patient.id")
            .select('comment.*', 'user_patient.username')
            .where('doctor_profile_id', doctorID)
            .orderBy('id','desc')
        return doctorInfo;
    }

    async createComment(doctorID: number, comment: string, patientID: number) {
        const commentID = await this.knex(commentTableName)
            .insert([{ comment: comment, patient_id: patientID, doctor_profile_id: doctorID }])
            .returning('id')
        return commentID
    }
    async getRating(doctorID: number) {
        const rating = await this.knex(doctorProfileTableName)
            .select('rating')
            .where('id', doctorID)
        return rating;
    }
    async getCommentCount(doctorID: number) {
        const commentCount = await this.knex(doctorProfileTableName)
            .select('comment_count')
            .where('id', doctorID)
        return commentCount
    }
    async updateRating(doctorID: number, rating: number) {
        const data = await this.knex(doctorProfileTableName)
            .update({ rating: rating })
            .where('id', doctorID)
        return data
    }
    async updateCommentCount(doctorID: number, comment_count: number) {
        const data = await this.knex(doctorProfileTableName)
            .update({ comment_count: comment_count })
            .where('id', doctorID)
        return data
    }
    async bookmarkDoctor(doctorID: number, patientID: number) {
        const id = await this.knex("patient_bookmark")
            .insert({
                'patient_id': patientID,
                'doctor_profile_id': doctorID
            })
        return id
    }

    async cancelBookmark(patientID:number, doctorID:number) {
        await this.knex("patient_bookmark")
        .where("patient_id", patientID)
        .andWhere("doctor_profile_id", doctorID)
        .del()
        return
    }

    async getPatientID(userID: number) {
        const id = await this.knex(patientProfileTableName)
            .select("id")
            .where('user_patient_id', userID)
        return id
    }
    async getBookmarkDoctor(patientID: number) {
        const sqlResult = await this.knex("patient_bookmark")
            .innerJoin("doctor_profile", "patient_bookmark.doctor_profile_id", "doctor_profile.id")
            .innerJoin("doctor_specialty", "doctor_specialty.doctor_profile_id", "doctor_profile.id")
            .innerJoin("specialty", "doctor_specialty.specialty_id", "specialty.id")
            .select("doctor_profile.*",
                "specialty.specialty")
            .where("patient_bookmark.patient_id", patientID)
        console.log(sqlResult)
        let groupedDoctors = new Map();
        for (let item of sqlResult) {
            const specialty = {
                specialty: item.specialty
            };
            if (groupedDoctors.has(item.registration_no)) {
                groupedDoctors.get(item.registration_no).specialties.push(specialty);
            } else {
                const doctor = {
                    id: item.id,
                    name: item.name,
                    profile_pic: item.profile_pic,
                    rating: item.rating,
                    specialties: [specialty],
                };
                groupedDoctors.set(item.registration_no, doctor)
            }
        }
        const result = Array.from(groupedDoctors.values());
        return result;
    }

    async checkQueueActive(clinicID: number) {
        const status = await this.knex(clinicProfileTableName)
            .select('queue_active').where('id', clinicID)

        return status
    }

    async checkBookmarkDoctor(doctorID: number, patientID: number) {
        console.log("hi")
        const id = await this.knex("patient_bookmark")
            .select("*")
            .where("patient_id", patientID)
            .andWhere("doctor_profile_id", doctorID)
        return id
    }

    async getDoctorName(userID: number) {
        const doctorName = await this.knex("doctor_profile")
            .select("name")
            .where("user_medical_id", userID)
        return doctorName
    }
    async getPatientProfileById(patientID: number) {
        const patientInfo = await this.knex("patient_profile")
            .select("name", "gender")
            .where("id", patientID)
        return patientInfo
    }

    // async getUsername (commentID: number) {
    //     const username = await this.knex("comment")
    //     .innerJoin("patient_profile", "comment.patient_id", "patient_profile.id")
    //     .innerJoin("user_patient", "patient_profile.user_medical_id", "user_patient.id")
    //     .select("user_patient.username")
    //     return username[0]
    // }
}