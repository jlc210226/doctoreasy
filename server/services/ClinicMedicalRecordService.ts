import Knex from 'knex'
import { doctorProfileTableName, diagnosisReportTableName } from "../utils/tables"

export class ClinicMedicalService {
    constructor(private knex: Knex) { }

    async clinicGetAllMedicalRecord(patientID: number) {
        const medicalInfo = await this.knex(diagnosisReportTableName)
            .innerJoin(doctorProfileTableName, `${diagnosisReportTableName}.doctor_profile_id`, 'doctor_profile.id',)
            .select(`${diagnosisReportTableName}.id`,`${doctorProfileTableName}.name`, `${diagnosisReportTableName}.diagnosis`, `${diagnosisReportTableName}.report_date`)
            .where('diagnosis_report.patient_id', patientID)
        console.log(`medicalInfo:${medicalInfo}`)
        return medicalInfo;
    }

    async createMedicalRecord(patientID:number, doctorID:number, clinicID:number, report_date: Date, symptoms: string, diagnosis: string, medication: string, remark:string) {
        const id = await this.knex(diagnosisReportTableName)
        .insert({patient_id: patientID, clinic_id: clinicID, doctor_profile_id: doctorID, report_date: report_date, symptoms: symptoms, diagnosis: diagnosis, medication: medication, remark: remark})
        return id;
    }
    async getClinicIDByDoctorID(doctorID:number) {
        const clinicID = await this.knex("user_medical")
        .select("clinic_profile_id")
        .where("id", doctorID)
        return clinicID;
    }

    async getDoctorProfileIDbyUserID(userID:number) {
        const docProfileID = await this.knex("doctor_profile")
        .select("id")
        .where("user_medical_id", userID)
        .first()
        return docProfileID
    }
}


