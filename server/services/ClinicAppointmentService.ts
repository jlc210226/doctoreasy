import Knex from 'knex';
// import redis from 'redis';
// import { redisClient } from "../main";
// import util from 'util';

export class ClinicAppointmentService {
    constructor(private knex: Knex) {
    }

    async updateAppointmentStatus(appointmentID: number, statusID: number) {

        const [appointment] = await this.knex
            ('booking_record').where({
                'appointment_number': appointmentID
            }).update(
                'status_id', statusID
            ).returning(['clinic_id'])

            

        return appointment;
    }

    async getAppointmentByappointmentID(appointmentID: number) {

        const [appointment] = await this.knex
        .column('booking_record.appointment_number', 'booking_status.status', { doctor_name: 'doctor_profile.name' }, { clinic_name: 'clinic_profile.name' },{ patient_name: 'patient_profile.name' },{ patient_id: 'patient_profile.id' } )
        .from('booking_record')
        .innerJoin('doctor_profile', 'doctor_profile.id', 'booking_record.doctor_profile_id')
        .innerJoin('user_medical', 'user_medical.id', 'doctor_profile.user_medical_id')
        .innerJoin('clinic_profile', 'clinic_profile.id', 'user_medical.clinic_profile_id')
        .innerJoin('patient_profile', 'patient_profile.id', 'booking_record.patient_id')
        .innerJoin('booking_status', 'booking_record.status_id', "booking_status.id")
        .where('booking_record.appointment_number', appointmentID);

        return appointment;

    }

}

