import { hashPassword } from "../utils/hash";
import Knex from 'knex';
import { User }from "../utils/models";
import { userMedicalTableName } from "../utils/tables";

export class UserService {
    constructor(private knex: Knex){}    
    async getUserByUsername(username: string) {
            const user = await this.knex<User>('user_patient')
                .where('username', username)
                .first();
            return user;
    }

    async getMedicalUserByUsername(username: string) {
        const user = await this.knex<User>(userMedicalTableName)
            .where('username', username)
            .first();
        return user;
}

    async createUser(username: string, password: string) {
        const hashedPassword = await hashPassword(password);
        const [userID] = await this.knex('user_patient')
            .insert({
                username,
                password: hashedPassword,
            })
            .returning('id');
        return userID;
    }
    async createProfile(user_patient_id: number, name: string, date_of_birth: Date, address: string, gender: string, id_number: string, phone: string, email: string) { 
        console.log(name, date_of_birth, gender)
        const [profileID] = await this.knex('patient_profile')
            .insert({
                user_patient_id,
                name,
                date_of_birth,
                address,
                gender,
                id_number,
                phone,
                email
            })
            .returning('id');
        return profileID;
    }
}