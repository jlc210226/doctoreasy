import Knex from "knex";
import { Clinic, Doctor } from "../utils/models";
import { clinicProfileTableName, districtTableName, doctorProfileTableName, doctorSpecialtyTableName, specialtyTableName, ticketStatusTableName, ticketTableName, userMedicalTableName } from "../utils/tables";

export class SearchMedicalService {
    constructor(private knex: Knex) { }

    async getTopDoctors(): Promise<Doctor[]> {

        const topDoctors = await this.knex
            .select([
                `${doctorProfileTableName}.*`,
                `${specialtyTableName}.specialty`,
                `${districtTableName}.district`,
            ])
            .from(doctorProfileTableName)
            .innerJoin(doctorSpecialtyTableName, `${doctorSpecialtyTableName}.doctor_profile_id`, `${doctorProfileTableName}.id`)
            .innerJoin(specialtyTableName, `${doctorSpecialtyTableName}.specialty_id`, `${specialtyTableName}.id`)
            .innerJoin(userMedicalTableName, `${doctorProfileTableName}.user_medical_id`, `${userMedicalTableName}.id`)
            .innerJoin(clinicProfileTableName, `${userMedicalTableName}.clinic_profile_id`, `${clinicProfileTableName}.id`)
            .innerJoin(districtTableName, `${clinicProfileTableName}.district_id`, `${districtTableName}.id`)
            .orderByRaw('rating DESC NULLS LAST')
            .limit(10)

        let groupedDoctors = new Map();
        for (let item of topDoctors) {
            const specialty = {
                specialty: item.specialty
            };

            if (groupedDoctors.has(item.registration_no)) {
                groupedDoctors.get(item.registration_no).specialties.push(specialty)
            } else {
                const doctor: Doctor = {
                    id: item.id,
                    name: item.name,
                    gender: item.gender,
                    registration_no: item.registration_no,
                    profile_pic: item.profile_pic,
                    user_medical_id: item.user_medical_id,
                    rating: item.rating,
                    comment_count: item.comment_count,
                    specialties: [specialty],
                    district: item.district
                };
                groupedDoctors.set(item.registration_no, doctor)
            }
        }

        const topDoctorProfiles = Array.from(groupedDoctors.values())

        return topDoctorProfiles;
    }

    async getDoctorById(id: number) {
        const [doctor] = await this.knex<Doctor>("doctor_profile")
            .where("id", id)

        return doctor;
    }

    async searchDoctor(orderBy: string, district: string, specialty: string, keywords: string) {

        const resultDoctors = await this.knex
            .select([
                `${doctorProfileTableName}.*`,
                `${specialtyTableName}.specialty`,
                `${districtTableName}.district`,
            ])
            .from(doctorProfileTableName)
            .innerJoin(doctorSpecialtyTableName, `${doctorSpecialtyTableName}.doctor_profile_id`, `${doctorProfileTableName}.id`)
            .innerJoin(specialtyTableName, `${doctorSpecialtyTableName}.specialty_id`, `${specialtyTableName}.id`)
            .innerJoin(userMedicalTableName, `${doctorProfileTableName}.user_medical_id`, `${userMedicalTableName}.id`)
            .innerJoin(clinicProfileTableName, `${userMedicalTableName}.clinic_profile_id`, `${clinicProfileTableName}.id`)
            .innerJoin(districtTableName, `${clinicProfileTableName}.district_id`, `${districtTableName}.id`)
            .orderByRaw(`${orderBy} DESC NULLS LAST`)
            .whereRaw(`LOWER(doctor_profile.name) LIKE LOWER('%${keywords}%')`)
            .andWhere(`${districtTableName}.district`, "LIKE", `%${district}%`)
            .andWhere(`${specialtyTableName}.specialty`, "LIKE", `%${specialty}%`)

        let groupedDoctors = new Map();
        for (let item of resultDoctors) {
            const specialty = {
                specialty: item.specialty
            };

            if (groupedDoctors.has(item.registration_no)) {
                groupedDoctors.get(item.registration_no).specialties.push(specialty)
            } else {
                const doctor = {
                    id: item.id,
                    name: item.name,
                    gender: item.gender,
                    registration_no: item.registration_no,
                    profile_pic: item.profile_pic,
                    user_medical_id: item.user_medical_id,
                    rating: item.rating,
                    comment_count: item.comment_count,
                    specialties: [specialty],
                    district: item.district
                };
                groupedDoctors.set(item.registration_no, doctor)
            }
        }

        const resultDoctorProfiles: Doctor[] = Array.from(groupedDoctors.values())

        return resultDoctorProfiles;

    }

    async searchClinic(district: string, keywords: string): Promise<Clinic[]> {

        const resultClinics = await this.knex(
            this.knex
                .column('*', { clinic_profile_id: `${clinicProfileTableName}.id` })
                .from(clinicProfileTableName)
                .innerJoin(districtTableName, `${clinicProfileTableName}.district_id`, `${districtTableName}.id`)
                .whereRaw(`LOWER(${clinicProfileTableName}.name) LIKE LOWER('%${keywords}%')`)
                .andWhere(`${districtTableName}.district`, "LIKE", `%${district}%`)
                .as("t1"))
            .leftJoin(
                this.knex.select(`${ticketTableName}.clinic_id`, `${ticketTableName}.ticket_number`)
                    .from(ticketTableName)
                    .innerJoin(ticketStatusTableName, `${ticketTableName}.status_id`, `${ticketStatusTableName}.id`)
                    .where(`${ticketTableName}.created_at`, ">", "'today'")
                    .andWhere(function() {
                        this.where(`${ticketStatusTableName}.status`, "BOOKED").orWhere(`${ticketStatusTableName}.status`, "SEEING_DOCTOR")
                    })
                    .as("t2"), "t1.clinic_profile_id", "t2.clinic_id"
            )

        console.log(resultClinics)

        let groupedClinics = new Map();
        for (let item of resultClinics) {
            const ticket = {
                ticket_number: item.ticket_number
            };

            if (groupedClinics.has(item.name)) {
                groupedClinics.get(item.name).tickets.push(ticket)
            } else {
                const clinic = {
                    id: item.clinic_profile_id,
                    name: item.name,
                    tickets: [ticket],
                    district: item.district,
                    queue_active: item.queue_active,
                };
                groupedClinics.set(item.name, clinic)
            }
        }

        const resultClinicProfiles: Clinic[] = Array.from(groupedClinics.values())

        return resultClinicProfiles;
    }

    async searchPatient(clinicID: number, keywords: string) {
        console.log('service')
        const resultPatient = await this.knex
            .column('booking_record.clinic_id', 'booking_record.doctor_profile_id',
                'booking_record.appointment_number', 'booking_record.date_of_booking',
                'booking_record.time_of_booking', { patient_name: 'patient_profile.name' },
                'booking_record.patient_id', { doctor_name: 'doctor_profile.name' }, 'booking_status.status',
                { clinic_name: 'clinic_profile.name' })
            .from('booking_record')
            .innerJoin('clinic_profile', 'clinic_profile.id', 'booking_record.clinic_id')
            .innerJoin('doctor_profile', 'doctor_profile.id', 'booking_record.doctor_profile_id')
            .innerJoin('patient_profile', 'patient_profile.id', 'booking_record.patient_id')
            .innerJoin('booking_status', 'booking_status.id', 'booking_record.status_id')
            .whereRaw(`LOWER(patient_profile.name) LIKE LOWER('%${keywords}%')`)
            .andWhere('booking_record.clinic_id', clinicID)

        console.log(resultPatient)
        return resultPatient;
    }
    
}
