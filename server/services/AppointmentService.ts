import Knex from "knex";
import { patientProfileTableName } from "../utils/tables";
import { bookingRecordTableName } from '../utils/tables'
// import { tables } from "../utils/tables";
// import { TodoItem } from "./model";



export class AppointmentService {
    constructor(private knex: Knex) { }

    async createAppointment(userID: number, doctorID: number, date_of_booking: any, time_of_booking: any) {
        try {
            console.log('created appointment')

            // 
            const [clinicID]: any = await this.knex
                .select('user_medical.clinic_profile_id', 'doctor_profile.name')
                .from('doctor_profile')
                .innerJoin('user_medical', 'user_medical.id', 'doctor_profile.user_medical_id')
                .innerJoin('clinic_profile', 'clinic_profile.id', 'user_medical.clinic_profile_id')
                .where('doctor_profile.id', doctorID)
                .returning('*')
            // console.log(clinicID)
            // console.log(clinicID.clinic_profile_id)

            let patientID = await this.knex(patientProfileTableName).where({
                user_patient_id: userID
            }).select('id');

            patientID = patientID[0].id;
            const newDate = new Date();

            const bookingCount = await this.knex
            .count('*')
            .from('booking_record')
            .where('patient_id', patientID)
            .andWhere('doctor_profile_id', doctorID)
            .andWhere('status_id', '<=', '3')

            console.log(bookingCount)

            if (bookingCount[0].count > 0) {

                return {message: "Have booking already"};
            }
            
            const [appointmentInfo] = await this.knex('booking_record')
                .insert({
                    'ticket_number': null,
                    'appointment_number': `${patientID}${Math.floor(newDate.getTime()/100000)}`,
                    'patient_id': patientID,
                    'clinic_id': clinicID.clinic_profile_id,
                    'doctor_profile_id': doctorID,
                    'date_of_booking': date_of_booking,
                    'time_of_booking': time_of_booking,
                    'status_id': 1,
                })
                .returning('*');

            const [patientInfo]: any = await this.knex
                .select('patient_profile.name')
                .from('patient_profile')
                .where('patient_profile.id', patientID)
                .returning('*')

            // const [status]: any = await this.knex
            // .select('booking_record.status_id', 'booking_status.status')
            // .from('booking_record')
            // .innerJoin('booking_status', 'booking_status.id', 'booking_record.status_id')
            // .where('booking_record.patient_id', patientID)
            // .returning('*')

            return { appointmentInfo, clinicID, patientInfo };

            // const appointment_number = await this.knex.raw(`insert into booking_record(date_of_booking,time_of_booking) values (${date_of_booking},${time_of_booking})`);

            // const insertClinicID = await this.knex('booking_record')
            //     .where({'doctor_profile_id': doctorID})
            //     .update({
            //         'clinic_id': clinicID,
            //     })
            // console.log(insertClinicID)

        } catch (ex) {
            console.log(ex);
            return 0;
        }
    }

    async getTimeSlot(doctorID: number) {
        // const [availableTimeSlot] = await this.knex('booking_record')
        //     .innerJoin('doctor_profile', 'booking_record.doctor_profile_id', 'doctor_profile.id')
        //     .innerJoin('doctor_office_hour', 'doctor_profile.id', 'doctor_office_hour.doctor_profile_id')
        //     .select('start', 'end')
        //     .where('booking_record.id')
        const allTime = await this.knex
            .select("*")
            .from("doctor_profile")
            .innerJoin("doctor_office_hour", "doctor_office_hour.doctor_profile_id", "doctor_profile.id")
            .where(
                'doctor_profile.id', doctorID
            )
            .returning('*')

        console.log(allTime)

        // const [query1] = await this.knex
        //     .select("doctor_profile.name", "doctor_office_hour.start", "doctor_office_hour.end")
        //     .from("doctor_profile")
        //     .innerJoin("doctor_office_hour", "doctor_office_hour.doctor_profile_id", "doctor_profile.id")
        //     .where(
        //         'doctor_profile.id', doctorID
        //     )
        //     .returning('*')
        // console.log(query1)

        // const [query2] = await this.knex
        //     .select("doctor_profile.name", "doctor_office_hour.start", "doctor_office_hour.end")
        //     .from("doctor_profile")
        //     .innerJoin("doctor_office_hour", "doctor_office_hour.doctor_profile_id", "doctor_profile.id")
        //     .where(
        //         'doctor_office_hour.day',
        //         selectDate
        //     )

        return [allTime];
    }

    async getDoctorInfo(doctorID: number) {
        const docInfo = await this.knex
            .select('doctor_profile.name', 'specialty.specialty', 'clinic_profile.id')
            .from('doctor_profile')
            .innerJoin('doctor_specialty', 'doctor_specialty.doctor_profile_id', 'doctor_profile.id')
            .innerJoin('specialty', 'specialty.id', 'doctor_specialty.specialty_id')
            .innerJoin('user_medical', 'user_medical.id', 'doctor_profile.user_medical_id')
            .innerJoin('clinic_profile', 'clinic_profile.id', 'user_medical.clinic_profile_id')
            .where('doctor_profile.id', doctorID);
        return docInfo;
    }

    async getConfirmationNumber(id: number) {
        const number = await this.knex('booking_record')
            .select('appointment_number', 'date_of_booking', 'time_of_booking')
            .where('id', id);
        return number;
    }

    async getAppointmentInfo(appointmentID: number) {
        console.log(`[service] ${appointmentID}`)

        const info = await this.knex
            // ('booking_record')
            // .select('date_of_booking', 'time_of_booking')
            // .where('id', appointmentID);
            // console.log(`[service] ${info}`)

            // .select('booking_record.appointment_number', 'booking_record.date_of_booking', 'booking_record.time_of_booking', 'doctor_profile.name', 'specialty.specialty', 'clinic_profile.name', 'clinic_profile.address', 'clinic_profile.phone')

            .column('booking_record.appointment_number', 'booking_record.date_of_booking', 'booking_record.time_of_booking', { doctor_name: 'doctor_profile.name' }, 'specialty.specialty', { clinic_name: 'clinic_profile.name' }, 'clinic_profile.address', 'clinic_profile.phone')
            .from('booking_record')
            .innerJoin('doctor_profile', 'doctor_profile.id', 'booking_record.doctor_profile_id')
            .innerJoin('doctor_specialty', 'doctor_specialty.doctor_profile_id', 'doctor_profile.id')
            .innerJoin('specialty', 'specialty.id', 'doctor_specialty.specialty_id')
            .innerJoin('user_medical', 'user_medical.id', 'doctor_profile.user_medical_id')
            .innerJoin('clinic_profile', 'clinic_profile.id', 'user_medical.clinic_profile_id')
            .where('booking_record.id', appointmentID);

        console.log(info)
        return info;

    }

    async getAllDoctorAppointment(doctorID: number) {
        console.log(`[service] ${doctorID}`)

        const info = await this.knex
            .select('date_of_booking', 'time_of_booking')
            .from('booking_record')
            .where('doctor_profile_id', doctorID)
        // .returning('*');

        console.log(info)
        return info;

    }

    async getAllAppointment(userID: number) {
        const patientID = await this.knex(patientProfileTableName)
            .where('user_patient_id', userID)
            .select('id')
            .first();
        console.log(patientID.id)
        const info = await this.knex("booking_record")
            .select('*', 'booking_record.id')
            .innerJoin('clinic_profile', 'booking_record.clinic_id', 'clinic_profile.id')
            .where('patient_id', patientID.id)
        console.log(info)
        return info
    }

    async getCurrentServingAppointments(clinicID: number) {

        const currentServingAppointments = await this.knex(bookingRecordTableName).where({
            'clinic_id': clinicID,
            'status_id': 3
        }).select('ticket_number');

        return currentServingAppointments;
    }

    async getLastServedAppointments(clinicID: number) {

        const from = new Date();
        from.setHours(6, 0, 0);

        const to = new Date(from)
        to.setDate(to.getDate() + 1);
        to.setHours(0, 0, 0);

        const lastServedAppointments = await this.knex(bookingRecordTableName).where({
            'clinic_id': clinicID,
            'status_id': 4
        }).whereBetween('booking_record.created_at', [from, to]).select('ticket_number');

        return lastServedAppointments;
    }

    async updateBookingStatus(clinicID: number, bookingID: number, statusID: number) {

        await this.knex(bookingRecordTableName).where({
            'clinic_id': clinicID,
            'id': bookingID
        }).update(
            'status_id', statusID
        )

        return;
    }

    async getAppointmentInfoforClinic(clinicID: number) {
        console.log(`[service] ${clinicID}`)

        const info = await this.knex
            .column('booking_record.clinic_id', 'booking_record.doctor_profile_id', 'booking_record.appointment_number', 'booking_record.date_of_booking', 'booking_record.time_of_booking', { patient_name: 'patient_profile.name' }, 'booking_record.patient_id', { doctor_name: 'doctor_profile.name' }, { status: 'booking_status.id' }, { status_name: 'booking_status.status' }, { clinic_name: 'clinic_profile.name' })
            .from('booking_record')
            .innerJoin('clinic_profile', 'clinic_profile.id', 'booking_record.clinic_id')
            .innerJoin('doctor_profile', 'doctor_profile.id', 'booking_record.doctor_profile_id')
            .innerJoin('patient_profile', 'patient_profile.id', 'booking_record.patient_id')
            .innerJoin('booking_status', 'booking_status.id', 'booking_record.status_id')
            .where('booking_record.clinic_id', clinicID);

        console.log(info)
        return info;

    }
}
