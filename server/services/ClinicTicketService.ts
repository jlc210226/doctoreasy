import Knex from 'knex';
import { clinicProfileTableName, ticketTableName, ticketStatusTableName, patientProfileTableName } from "../utils/tables";
import { redisClient } from "../main";
import util from 'util';


export class ClinicTicketService {
    constructor(private knex: Knex) {
    }

    async updateTicketStatus(ticketID: number, statusID: number) {

        const ticket = await this.knex(ticketTableName).where({
            'id': ticketID
        }).update(
            'status_id', statusID
        ).returning('*')

        return ticket;
    }

    async getReadyTickets(clinicID: number) {

        const readyTickets = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 1
        }).where('ticket.created_at', ">", "'today'")
            .select('ticket_number').limit(3).orderBy('id')

        return readyTickets;
    }

    async clinicGetCurrentServingTicket(clinicID: number) {

        const currentServingTickets = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 2
        }).where('ticket.created_at', ">", "'today'").select('ticket_number')

        return currentServingTickets;
    }

    async clinicGetTicket(clinicID: number) {

        // Set UTC C
        const from = new Date();
        from.setDate(from.getDate() - 1)
        from.setUTCHours(22, 0, 0);

        const to = new Date()
        to.setDate(to.getDate());
        to.setUTCHours(16, 0, 0);


        const tickets = await this.knex(
            this.knex
                .from(ticketTableName)
                .column('*', { ticket_id: 'ticket.id' }, { ticket_created_at: 'ticket.created_at' })
                .innerJoin(ticketStatusTableName, "ticket_status.id", "ticket.status_id")
                .innerJoin(patientProfileTableName, "patient_profile.id", "ticket.patient_id")
                .andWhereBetween('ticket.created_at', [from, to])
                .andWhere("ticket_status.status", "=", "BOOKED")
                .orWhere("ticket_status.status", "=", "SEEING_DOCTOR")
                .as("t1"))
            .select('*')
            .where("clinic_id", "=", clinicID)
            .orderBy('ticket_number')

        // const tickets = await this.knex(ticketTableName)
        //     .column('*', { ticket_id: 'ticket.id' }, { ticket_created_at: 'ticket.created_at' })
        //     .innerJoin(ticketStatusTableName, "ticket_status.id", "ticket.status_id")
        //     .innerJoin(patientProfileTableName, "patient_profile.id", "ticket.patient_id")
        //     .andWhereBetween('ticket.created_at', [from, to])
        //     .whereRaw("ticket_status.status =  'BOOKED' OR ticket_status.status =  'SEEING DOCTOR' ")
        //     // .andWhere("ticket_status.status", "=", "BOOKED")
        //     // .orWhere("ticket_status.status", "=", "SEEING_DOCTOR")
        //     .andWhere("ticket.clinic_id", "=", clinicID)
        //     .orderBy('ticket.ticket_number')

        return tickets;

    }

    async startTicketQueue(clinicID: number) {

        await this.knex(clinicProfileTableName).update({
            'queue_active': true
        }).where('id', clinicID)

        return;
    }

    async stopTicketQueue(clinicID: number) {

        await this.knex(clinicProfileTableName).update({
            'queue_active': false
        }).where('id', clinicID)

        return;
    }

    async clinicGetTodayTickets(clinicID: number) {


        const tickets = await this.knex(ticketTableName)
            .column('*', { ticket_id: 'ticket.id' }, { ticket_created_at: 'ticket.created_at' })
            .innerJoin(ticketStatusTableName, "ticket_status.id", "ticket.status_id")
            .innerJoin(patientProfileTableName, "patient_profile.id", "ticket.patient_id")
            .where("ticket.clinic_id", clinicID)
            .where('ticket.created_at', ">", "'today'")
            .orderBy('ticket.ticket_number')


        return tickets;

    }

    async clinicSetTicketNumber(clinicID: number, ticketNumber: string) {

        const redisSet = util.promisify(redisClient.set).bind(redisClient)
        const redistExpireat = util.promisify(redisClient.expireat).bind(redisClient)

        await redisSet(`${clinicID}`, `${ticketNumber}`);

        // Set redis expire time

        let endTime = new Date().setUTCHours(15, 59, 59, 999);
        endTime = Math.floor(endTime / 1000)

        await redistExpireat(`${clinicID}`, endTime)

        return ticketNumber;
    }

    async clinicGetNextTicketNumber(clinicID: number) {
        const redisGet = util.promisify(redisClient.get).bind(redisClient)
        const redisSet = util.promisify(redisClient.set).bind(redisClient)
        const redistExpireat = util.promisify(redisClient.expireat).bind(redisClient)

        let nextTicket = await redisGet(`${clinicID}`)

        if (nextTicket === null) {
            await redisSet(`${clinicID}`, '1');

            // Set redis expire time

            let endTime = new Date().setUTCHours(15, 59, 59, 999);
            endTime = Math.floor(endTime / 1000)

            await redistExpireat(`${clinicID}`, endTime)

            nextTicket = '1';
            return nextTicket;
        }

        return nextTicket;

    }

    async clinicGetLastServedTicket(clinicID: number) {

        const lastServedTicket = await this.knex(ticketTableName).where({
            'clinic_id': clinicID,
            'status_id': 3
        }).where('ticket.created_at', ">", "'today'").select('ticket_number')

        return lastServedTicket;
    }

}

