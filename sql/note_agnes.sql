-- Start For ProfileService

SELECT * from doctor_profile
        INNER JOIN user_medical
        ON doctor_profile.user_medical_id = user_medical.id
        INNER JOIN clinic_profile
        ON clinic_profile.id = user_medical.clinic_profile_id
        WHERE clinic_profile.id = 1;

-- if you wanna select specific column, you need to use 'tableName.columnName'
-- Otherwise, will pop below ERROR
-- -- ERROR:  column reference "name" is ambiguous
-- -- LINE 1: SELECT name from doctor_profile
--                ^

SELECT doctor_profile.name from doctor_profile
        INNER JOIN user_medical
        ON doctor_profile.user_medical_id = user_medical.id
        INNER JOIN clinic_profile
        ON clinic_profile.id = user_medical.clinic_profile_id
        WHERE clinic_profile.id = 1;

select "doctor_profile"."name" from "doctor_profile" 
inner join "user_medical"
on "doctor_profile"."user_medical_id" = "user_medical"."id" 
inner join "clinic_profile" 
on "clinic_profile"."id" = "user_medical"."clinic_profile_id"
where "clinic_profile"."id" = $1 - invalid reference to FROM-clause entry for table "user_medical"






-- Start For Appointment Service

-- add new appointment
insert into booking_record (ticket_number, appointment_number, patient_id, clinic_id, doctor_profile_id, status_id) VALUES 
('2',4419623387, 21, 1, 1, 3)

-- 3 seeing doctor
UPDATE booking_record SET status_id = 3 where clinic_id = 1 AND ticket_number = '1';
UPDATE booking_record SET status_id = 3 where clinic_id = 1 AND ticket_number = '2';

-- 4 complete
UPDATE booking_record SET status_id = 4 where clinic_id = 1 AND ticket_number = '1';
UPDATE booking_record SET status_id = 4 where clinic_id = 1 AND ticket_number = '2';






-- Start For Ticket Service

-- booked
UPDATE ticket SET status_id = 1 where clinic_id = 1 AND ticket_number = '2';

-- 2 seeing doctor

UPDATE ticket SET status_id = 2 where clinic_id = 1 AND ticket_number = '1';
UPDATE ticket SET status_id = 2 where clinic_id = 1 AND ticket_number = '2';

-- 3 complete

UPDATE ticket SET status_id = 3 where clinic_id = 1 AND ticket_number = '1';
UPDATE ticket SET status_id = 3 where clinic_id = 1 AND ticket_number = '2';


-- before add ticket, need to see which is the latest ticket
SELECT ticket_number FROM ticket WHERE clinic_id = 1 AND status_id = 1 ORDER BY ticket_number DESC LIMIT 1


insert into ticket (ticket_number, patient_id, clinic_id, status_id) VALUES 
('2', 21, 1, 1, 3)

insert into `books` (`title`) values ('Slaughterhouse Five')


SELECT ticket.ticket_number, ticket.created_at, clinic_profile.name, clinic_profile.address, clinic_profile.opening_hours 
FROM ticket INNER JOIN clinic_profile on
ticket.clinic_id = clinic_profile.id
INNER JOIN doctor_profile
ON doctor_profile.
where ticket.id = 35;


WHERE ticket_id = 1 


SELECT doctor_profile.name from doctor_profile
        INNER JOIN user_medical
        ON doctor_profile.user_medical_id = user_medical.id
        INNER JOIN clinic_profile
        ON clinic_profile.id = user_medical.clinic_profile_id
        WHERE clinic_profile.id = 1;


-- End For Ticket Service

UPDATE clinic_profile SET queue_active = false where id = 1;
UPDATE clinic_profile SET queue_active = true where id = 1;


-- Select Today Ticket by userID
SELECT *
FROM ticket
WHERE patient_id = 1 AND created_at >= NOW() - INTERVAL '24 HOURS' AND created_at <= NOW();
ORDER BY "created_at" DESC


SELECT ticket_number from ticket where clinic_id = 1 AND status_id = 1 ORDER BY ticket_number LIMIT 3;

SELECT * FROM ticket INNER JOIN ticket_status
ON ticket.status_id = ticket_status.id
INNER JOIN patient_profile
ON ticket.patient_id = patient_profile.id
WHERE ticket.clinic_id = 1 AND ticket.created_at >= NOW() - INTERVAL '24 HOURS' AND ticket.created_at <= NOW(); 

SELECT * from ticket WHERE patient_id = 1 AND status_id = 2 ORDER BY created_at DESC LIMIT 1