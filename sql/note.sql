CREATE DATABASE doctor_easy

CREATE DATABASE doctor_easy_test

INSERT INTO doctor_office_hour (
    doctor_profile_id, 
    day, 
    period, 
    start
)
VALUES (1, 1, 'morning', '11:00'),
(1, 1, 'morning', '12:00'),
(1, 1, 'afternoon', '16:00'),
(1, 1, 'afternoon', '17:00');